# jlpmud - A LPMud Driver in Java

![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/lycis%2Fjlpmud)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=lycis_jlpmud&metric=coverage)](https://sonarcloud.io/summary/new_code?id=lycis_jlpmud)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=lycis_jlpmud&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=lycis_jlpmud)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=lycis_jlpmud&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=lycis_jlpmud)
![GitLab License](https://img.shields.io/gitlab/license/lycis%2Fjlpmud)

`jlpmud` is an experimental project aimed at re-implementing the LPMud driver in Java. This project is primarily for enhancing my skills in Java and is not intended for production use.

## Project Overview
LPMud (Lars Pensjö's MUD) is a type of MUD (Multi-User Dungeon) that originated in the late 1980s. This project aims to bring a modern touch to LPMud by re-implementing its driver in Java.

## Features

- Purely experimental and educational project.
- Re-implementation of the LPMud driver.
- Written entirely in Java.

## Getting Started

### Prerequisites

- Java Development Kit (JDK) 8 or higher.
- Git.

### Installation

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/lycis/jlpmud.git
   ```
2. Navigate to the project directory:
   ```sh
    cd jlpmud
    ```
3. Build the project using Maven:
    ```sh
    mvn clean install
   ```
## Usage

Since this project is experimental, the usage instructions will evolve as the project progresses. For now, you can compile and run the main driver class.
Currently it cannot be executed and contains only a very basic implementation of the LPC interpreter. Only the tests are working.
## Contributing
Contributions are welcome! Feel free to fork the repository and submit merge requests.

1. Fork the repository.
2. Create a new branch (git checkout -b feature-branch).
3. Make your changes.
4. Commit your changes (git commit -am 'Add some feature').
5. Push to the branch (git push origin feature-branch).
6. Create a new Merge Request.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments
* Inspired by the original LPMud driver. Thanks to Lars Pensjö for creating it.