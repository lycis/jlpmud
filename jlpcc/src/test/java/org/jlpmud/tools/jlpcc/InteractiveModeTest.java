package org.jlpmud.tools.jlpcc;

import org.jlpmud.lpc.LPCInterpreter;
import org.jlpmud.lpc.LPCObject;
import org.jlpmud.lpc.LPCVariable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class InteractiveModeTest {
    private LPCObject lpcObjectMock;
    private LPCInterpreter lpcInterpreterMock;

    @BeforeEach
    void setUp() {
        lpcObjectMock = mock(LPCObject.class);
        LPCVariable variableMock = mock(LPCVariable.class);
        when(lpcObjectMock.getVariable("myVar")).thenReturn(variableMock);
        when(variableMock.toString()).thenReturn("mockedVariableValue");

        lpcInterpreterMock = mock(LPCInterpreter.class);
        Main.interpreter = lpcInterpreterMock;
    }

    @AfterEach
    void after() {
        Mockito.reset(lpcObjectMock, lpcInterpreterMock);
    }

    private ByteArrayOutputStream setUpOutputCapture() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        return outContent;
    }

    private void restoreOutput() {
        System.setOut(System.out);
    }

    private void testInteractWithObject(String inputCommand, String expectedOutput) {
        ByteArrayOutputStream outContent = setUpOutputCapture();
        Scanner scannerMock = mock(Scanner.class);
        String[] cmds = inputCommand.split("\n");
        if(!cmds[cmds.length - 1].equals("!exit")) {
            cmds = Arrays.copyOf(cmds, cmds.length + 1);
            cmds[cmds.length - 1] = "!exit";
        }
        when(scannerMock.nextLine()).thenReturn(cmds[0], Arrays.copyOfRange(cmds, 1, cmds.length));

        try {
            Main.interactWithObject(lpcObjectMock, scannerMock);
            assertThat(outContent.toString()).contains(expectedOutput);
        } finally {
            restoreOutput();
        }
    }

    @Test
    void testInteractWithObjectDumpCommand() {
        testInteractWithObject("!d\n!exit\n", lpcObjectMock.toString());
    }

    @Test
    void testInteractWithObjectPrintCommand() {
        testInteractWithObject("!print myVar\n!exit\n", "mockedVariableValue");
    }

    @Test
    void testInteractWithObjectHelpCommand() {
        testInteractWithObject("!help\n!exit\n", "Available directives:");
    }

    @Test
    void testInteractiveModeInjectCode() {
        LPCObject obj = mock(LPCObject.class);

        when(lpcInterpreterMock.interpretProgramAsObject(anyString())).thenReturn(obj);
        testInteractWithObject("int intVar = 19877678;\n", "");

        ArgumentCaptor<String> injectedCoreCaptor = ArgumentCaptor.forClass(String.class);
        verify(lpcInterpreterMock).interpretProgramAsObject(injectedCoreCaptor.capture());

        assertThat(injectedCoreCaptor.getValue()).contains("int intVar = 19877678;");
    }

    @Test
    void commentsAreIgnored() {
        LPCObject obj = mock(LPCObject.class);
        when(lpcInterpreterMock.interpretProgramAsObject(anyString())).thenReturn(obj);
        testInteractWithObject("// this is a comment\n", "");
        verify(lpcInterpreterMock , times(0)).interpretProgramAsObject(anyString());
    }

    @Test
    void emptyDebugerDirectiveDoesNothing() {
        ByteArrayOutputStream outContent = setUpOutputCapture();
        Main.debuggerDirective("", lpcObjectMock);
        assertThat(outContent.toString()).isEmpty();
        restoreOutput();
    }
}
