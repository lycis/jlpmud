package org.jlpmud.tools.jlpcc;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.jlpmud.lpc.LPCInterpreter;
import org.jlpmud.lpc.LPCObject;
import org.jlpmud.lpc.LPCVariable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class MainTest {

    @BeforeEach
    void setUp() {
        Main.interpreter = new LPCInterpreter();
    }

    @Test
    void shouldCreateCommandLineOptionsWithHelpAndInteractiveFlags() {
        Options options = Main.createCommandLineOptions();

        assertThat(options).isNotNull();
        assertThat(options.hasOption("h")).isTrue();
        assertThat(options.hasOption("i")).isTrue();
    }

    @Test
    void shouldParseValidCommandLineArgs() {
        String[] args = {"-i", "file1.lpc"};
        Options options = Main.createCommandLineOptions();
        CommandLine cmd = Main.parseCommandLineArgs(options, args);

        assertThat(cmd).isNotNull();
        assertThat(cmd.hasOption("i")).isTrue();

        List<String> files = cmd.getArgList();
        assertThat(files).hasSize(1).contains("file1.lpc");
    }

    @Test
    void shouldReturnNullForInvalidCommandLineArgs() {
        String[] args = {"-x"};
        Options options = Main.createCommandLineOptions();
        CommandLine cmd = Main.parseCommandLineArgs(options, args);

        assertThat(cmd).isNull();
    }

    @Test
    void shouldDoNothingWhenNoFilesProvidedForProcessing() {
        List<String> files = List.of();

        // Capture the original System.out
        PrintStream originalOut = System.out;

        // Create a ByteArrayOutputStream to capture the output to stdout
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            Main.processFiles(files, false);

            assertThat(outContent.toString().trim()).contains("No files provided for interpretation.");
        } finally {
            // Restore the original System.out
            System.setOut(originalOut);
        }
    }

    @Test
    void shouldProcessFilesAndInterpretProgramAsLPCObject() {
        // Mock LPCInterpreter's static method
        var interpreterMock = mock(LPCInterpreter.class);
        LPCObject lpcObjectMock = mock(LPCObject.class);
        when(interpreterMock.interpretProgramAsObject(anyString())).thenReturn(lpcObjectMock);

        Main.interpreter = interpreterMock;
        Main.originalCode = "mock LPC code";

        List<String> files = List.of("file1.lpc");
        Main.processFiles(files, false);

        verify(interpreterMock).interpretProgramAsObject(Main.originalCode);
    }

    @Test
    void shouldReturnNullWhenIOExceptionOccursWhileReadingFile() {
        String fileName = "nonexistentfile.lpc";
        String result = Main.readFileContent(fileName);

        assertThat(result).isNull();
    }

    @Test
    void shouldReadFileContentSuccessfully() {
        String fileName = "testfile.lpc";
        String expectedContent = "mock LPC content";

        // Mocking FileUtils to return expected content when readFileToString is called
        // You need to mock the static method, which requires a different setup
        try (MockedStatic<FileUtils> mockedStatic = mockStatic(FileUtils.class)) {
            mockedStatic.when(() -> FileUtils.readFileToString(any(File.class), eq(StandardCharsets.UTF_8)))
                    .thenReturn(expectedContent);

            // Call the method under test
            String result = Main.readFileContent(fileName);

            // Assert the result is as expected
            assertThat(result).isEqualTo(expectedContent);
        }
    }


    @Test
    void shouldInjectAdditionalCodeAndRunInteractiveFunction() {
        var interpreterMock = mock(LPCInterpreter.class);
        LPCObject lpcObjectMock = mock(LPCObject.class);

        when(interpreterMock.interpretProgramAsObject(anyString())).thenReturn(lpcObjectMock);

        Main.interpreter = interpreterMock;
        Main.originalCode = "mock original LPC code";
        String input = "some LPC code";

        LPCObject resultObject = Main.injectAdditionalCodeAndRunInteractiveFunction(input, lpcObjectMock);

        assertThat(resultObject).isNotNull();
        verify(lpcObjectMock).getFunction(anyString());
    }

    @Test
    void shouldExecuteDebuggerDirectiveAndPrintVariableValue() {
        LPCObject lpcObjectMock = mock(LPCObject.class);
        LPCVariable variableMock = mock(LPCVariable.class);
        when(lpcObjectMock.getVariable("myVariable")).thenReturn(variableMock);
        when(variableMock.toString()).thenReturn("mockVariableValue");

        String result = Main.printVariable("myVariable", lpcObjectMock);

        assertThat(result).isEqualTo("mockVariableValue");
    }

    @Test
    void shouldHandleUndefinedVariableInDebuggerDirective() {
        LPCObject lpcObjectMock = mock(LPCObject.class);
        when(lpcObjectMock.getVariable("undefinedVariable")).thenReturn(null);

        String result = Main.printVariable("undefinedVariable", lpcObjectMock);

        assertThat(result).isEqualTo("undefined");
    }

    @Test
    void shouldHandleDumpDirective() {
        LPCObject lpcObjectMock = mock(LPCObject.class);
        when(lpcObjectMock.toString()).thenReturn("mockedLPCObjectString");

        // Capture the original System.out
        PrintStream originalOut = System.out;

        // Create a ByteArrayOutputStream to capture the output to stdout
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            // Test the "dump" directive - expected to call toString and print to stdout
            Main.debuggerDirective("dump", lpcObjectMock);
            assertThat(outContent.toString()).contains("mockedLPCObjectString");

        } finally {
            // Restore the original System.out
            System.setOut(originalOut);
        }
    }

    @Test
    void shouldHandlePrintDirective() {
        LPCVariable mockVariable = mock(LPCVariable.class);
        LPCObject lpcObjectMock = mock(LPCObject.class);
        when(mockVariable.toString()).thenReturn("mockedVariableValue");
        when(lpcObjectMock.getVariable("myVar")).thenReturn(mockVariable);

        // Capture the original System.out
        PrintStream originalOut = System.out;

        // Create a ByteArrayOutputStream to capture the output to stdout
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            // Test the "print myVar" directive - expected to print the variable's value
            Main.debuggerDirective("print myVar", lpcObjectMock);
            assertThat(outContent.toString()).contains("mockedVariableValue");

        } finally {
            // Restore the original System.out
            System.setOut(originalOut);
        }
    }

    @Test
    void shouldHandleCodeDirective() {
        LPCObject lpcObjectMock = mock(LPCObject.class);

        // Capture the original System.out
        PrintStream originalOut = System.out;

        // Create a ByteArrayOutputStream to capture the output to stdout
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        Main.currentlyInterpretedCode = "Expected code output";

        try {
            // Test the "code" directive - verify the expected code output
            Main.debuggerDirective("code", lpcObjectMock);
            assertThat(outContent.toString()).contains("Expected code output");

        } finally {
            // Restore the original System.out
            System.setOut(originalOut);
        }
    }

}
