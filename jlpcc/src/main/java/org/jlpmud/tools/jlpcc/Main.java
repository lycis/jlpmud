package org.jlpmud.tools.jlpcc;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.jlpmud.lpc.LPCInterpreter;
import org.jlpmud.lpc.LPCObject;
import org.jlpmud.lpc.LPCVariable;
import org.jlpmud.lpc.error.LPCRunTimeException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String INTERACTIVE_FUNCTION_NAME = "__interactive_dbg_" + System.currentTimeMillis();
    static String functionCode;
    static String originalCode;
    static String currentlyInterpretedCode;
    static LPCInterpreter interpreter = new LPCInterpreter();

    public static void main(String[] args) {
        Options options = createCommandLineOptions();
        CommandLine cmd = parseCommandLineArgs(options, args);

        if (cmd == null) {
            return; // An error occurred during parsing
        }

        if (cmd.hasOption("h")) {
            printHelp(options);
            return;
        }

        List<String> files = cmd.getArgList();
        processFiles(files, cmd.hasOption("i"));
    }

    static Options createCommandLineOptions() {
        Options options = new Options();
        options.addOption("h", "help", false, "Show help message");
        options.addOption("i", "interactive", false, "Run the code in interactive mode");
        return options;
    }

    static CommandLine parseCommandLineArgs(Options options, String[] args) {
        CommandLineParser parser = new DefaultParser();

        try {
            return parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Error parsing command-line options: " + e.getMessage());
            printHelp(options);
            return null;
        }
    }

    static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar jlpcc.jar [options] [files]", options);
        System.out.println("\nCurrently, there are no further options.");
        System.out.println("This tool will interpret all the given files as LPC code and run them.");
    }

    static void processFiles(List<String> files, boolean interactiveMode) {
        boolean filePresent = !files.isEmpty();
        if (!filePresent && !interactiveMode) {
            System.out.println("No files provided for interpretation.");
            return;
        }

        if(filePresent) {
            originalCode = readFilesContent(files);
        } else {
            originalCode = "";
        }

        LPCObject obj = interpreter.interpretProgramAsObject(originalCode);

        if (interactiveMode) {
            interactWithObject(obj);
        }
    }

    static void interactWithObject(LPCObject inObject) {
        interactWithObject(inObject, new Scanner(System.in));
    }

    static void interactWithObject(LPCObject inObject, Scanner userInput) {
        System.out.println("Entering interactive mode. Type '!exit' to leave.");

        functionCode = "";
        currentlyInterpretedCode = originalCode;
        LPCObject obj = inObject;

        while (true) {
            System.out.print("LPC> "); // Prompt for LPC command input
            String input = userInput.nextLine().trim();

            if (input.equalsIgnoreCase("!exit")) {
                break; // Exit the interactive mode
            }

            if (input.startsWith("!")) {
                debuggerDirective(input.substring(1), obj);
            } else if (!input.isEmpty() && !input.startsWith("//")) {
                obj = injectAdditionalCodeAndRunInteractiveFunction(input, obj);
            }
        }

        userInput.close();
    }

    static LPCObject injectAdditionalCodeAndRunInteractiveFunction(String input, LPCObject obj) {
        String newFunctionCode = functionCode + "\n" + input;

        String injectedFunctionCode = "void " + INTERACTIVE_FUNCTION_NAME + "() {\n" + newFunctionCode + "\n}";
        try {
            obj = interpreter.interpretProgramAsObject(originalCode + "\n\n// injected by jlpcc interactive mode\n" + injectedFunctionCode);
            obj.getFunction(INTERACTIVE_FUNCTION_NAME).execute();
        } catch(LPCRunTimeException e) {
            System.out.println("Error at "+e.getLine()+":"+e.getPosition()+": " + e.getMessage());
            return obj;
        } catch (Exception e) {
            System.out.println("Error executing command: " + e.getMessage());
            return obj;
        }

        functionCode = newFunctionCode;
        currentlyInterpretedCode = originalCode + "\n" + injectedFunctionCode;
        return obj;
    }

    static void debuggerDirective(String line, LPCObject obj) {
        if(line.isEmpty()) {
            return;
        }

        String cmd;
        if(!line.contains(" ")) {
            cmd = line;
        } else {
            cmd = line.substring(0, line.indexOf(' '));
        }

        switch (cmd) {
            case "d", "dump":
                System.out.println(obj);
                break;
            case "print", "p":
                System.out.println(printVariable(line.substring(line.indexOf(' ') + 1), obj));
                break;
            case "code", "c":
                String[] lines = currentlyInterpretedCode.split("\n");
                for(int i = 0; i < lines.length; i++) {
                    System.out.printf("%04d: %s%n", i + 1, lines[i]);
                }
                break;
            case "help", "h":
                System.out.println("Available directives:");
                System.out.println("  d[ump] - Display the current state of the object");
                System.out.println("  h[elp] - Display this help message");
                System.out.println("  p[rint] <variable> - Print the value of a variable");
                System.out.println("  c[ode] - Print the current code");
                System.out.println("  exit - Exit the interactive mode");
                break;
            default:
                System.out.println("Unknown directive: " + line);
        }
    }

    static String printVariable(String substring, LPCObject obj) {
        LPCVariable variable = obj.getVariable(substring);
        if(variable == null) {
            return "undefined";
        }

        return obj.getVariable(substring).toString();
    }

    static String readFilesContent(List<String> files) {
        StringBuilder codeBuilder = new StringBuilder();
        for (String file : files) {
            String fileContent = readFileContent(file);
            if (fileContent != null) {
                codeBuilder.append(fileContent);
            }
        }
        return codeBuilder.toString();
    }

    static String readFileContent(String file) {
        try {
            return FileUtils.readFileToString(new File(file), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println("Error reading file: " + file + " error: " + e.getMessage());
            return null;
        }
    }
}
