string quote = "Hello, \"World\"!";
string newline = "Hello,\nWorld!";
string tab = "Hello,\tWorld!";
string backslash = "Hello, \\World!";
string carriageReturn = "Hello,\rWorld!";
string questionMark = "Hello, World\?";
string apostrophe = "Hello, \'World\'!";