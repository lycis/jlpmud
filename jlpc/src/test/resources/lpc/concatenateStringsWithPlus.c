string s1 = "a" + "b"; // -> "ab"
string s2 = "Hello, " + 1; // -> "Hello, 1"
string s3 = 7.5 + "g of apples"; // -> "7.5g of apples"
string s4 = 42 +" is the answer";
string s5 = "No, it is " + 4.2;