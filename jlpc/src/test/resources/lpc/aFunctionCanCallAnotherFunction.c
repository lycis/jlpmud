string world() {
  return "World";
}

string greeter() {
  return "Hello, " + world() + "!";
}

string str = greeter();
