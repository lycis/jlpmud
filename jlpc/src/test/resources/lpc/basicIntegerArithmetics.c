int multiplication = 3 * 11; // -> 33
int division = 33 / 3; // -> 11
int subtraction = 100 - 99; // -> 1
int addition = 99 +1; // -> 100
int modulo = 5%4; // -> 1
int complex = 3 + (3+2) * 2 - 4 / 2; // -> 11