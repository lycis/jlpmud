float fAdd = 0.1 + 0.1; // -> 0.2
float fSub = 0.5 - 0.1; // -> 0.4
float fMul = 0.1 * 10.0; // -> 1.0
float fDiv = 5.0 / 2.0; // -> 2.5
float complex = 1.0 - (2.0 * 0.4) / 2.0; // -> 0.6