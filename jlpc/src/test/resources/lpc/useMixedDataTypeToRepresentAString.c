mixed s = "Hello";

void func() {
    mixed who = "World";
    s = s + ", " + who + "!";
}