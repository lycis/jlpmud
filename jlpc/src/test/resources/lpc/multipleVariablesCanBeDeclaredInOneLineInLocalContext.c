int globA, globB, globC;

void multipleVariablesInFunction () {
    int a, b, c;
    a = 1;
    b = 2;
    c = 3;
    globA = a;
    globB = b;
    globC = c;
    return;
}