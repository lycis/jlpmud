// Global variables to store the results of bitwise operations
int bitwiseAnd;
int bitwiseOr;
int bitwiseXor;
int bitwiseNot;
int bitwiseShiftLeft;
int bitwiseShiftRight;

void func() {
    // Perform bitwise operations
    bitwiseAnd = 3 & 2; // 2
    bitwiseOr = 1 | 2; // 3
    bitwiseXor = 90^42; // 112
    bitwiseNot = ~7; // -8
    bitwiseShiftLeft = 3 << 2; // 12
    bitwiseShiftRight = 19 >> 1; // 9
}