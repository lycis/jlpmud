string greeter(string who, int nr, string how) {
  return "I greet "+who+" " + nr + " times "+how+"."; // -> "I greet XXX N times."
}

string str = greeter("you", 5, "friendly"); // -> "I greet you 5 times."
