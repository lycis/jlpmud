// Global variables to store the results of bitwise operations
int bitwiseAnd;
int bitwiseOr;
int bitwiseXor;
int bitwiseShiftLeft;
int bitwiseShiftRight;

void func() {
    // Perform bitwise operations
    bitwiseAnd = 3;
    bitwiseAnd &= 2; // 2

    bitwiseOr = 1 ;
    bitwiseOr |= 2; // 3

    bitwiseXor = 90;
    bitwiseXor ^= 42; // 112

    bitwiseShiftLeft = 3;
    bitwiseShiftLeft <<= 2; // 12

    bitwiseShiftRight = 19;
    bitwiseShiftRight >>= 1; // 9
}