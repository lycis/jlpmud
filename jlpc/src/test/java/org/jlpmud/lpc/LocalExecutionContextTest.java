package org.jlpmud.lpc;

import org.jlpmud.lpc.error.AssignmentTypeMismatch;
import org.jlpmud.lpc.error.InvalidDataTypeExpression;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigInteger;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class LocalExecutionContextTest {
    @Test
    void inheritVariableFromParentContext() throws InvalidDataTypeExpression {
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        Mockito.when(parent.getVariable("x")).thenReturn(LPCVariable.asInt("77"));

        LocalExecutionContext child = new LocalExecutionContext(parent);
        assertThat(child.getVariable("x").intValue()).isEqualTo(77);
    }

    @Test
    void canDefineALocalVariableIfItNotExistsInParentContext() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        Mockito.when(parent.getVariable("y")).thenReturn(null);

        LocalExecutionContext child = new LocalExecutionContext(parent);
        child.setVariable("y", LPCVariable.asInt("42"));

        Mockito.verify(parent, Mockito.never()).setVariable(Mockito.anyString(), Mockito.any(LPCVariable.class));
        assertThat(child.getVariable("y").intValue()).isEqualTo(42);
    }

    @Test
    void overwritesParentVariableIfDefinedInParent() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        LPCVariable mockParentVariable = Mockito.mock(LPCVariable.class);
        Mockito.when(parent.getVariable("z")).thenReturn(mockParentVariable);

        LocalExecutionContext child = new LocalExecutionContext(parent);
        child.setVariable("z", LPCVariable.asInt("77"));

        Mockito.verify(mockParentVariable, Mockito.times(1)).setValue(Mockito.argThat(v -> v.intValue().equals(BigInteger.valueOf(77))));
    }

    @Test
    void redefiningAParentVariableInChildDoesNotChangeParent() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        Mockito.when(parent.getVariable("z")).thenReturn(LPCVariable.asInt("42"));

        LocalExecutionContext child = new LocalExecutionContext(parent);
        child.setVariable("z", LPCVariable.asInt("77"));

        Mockito.verify(parent, Mockito.never()).setVariable(Mockito.anyString(), Mockito.any(LPCVariable.class));
    }

    @Test
    void passesFunctionsFromParentContext() {
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        Mockito.when(parent.hasFunction("test/foo")).thenReturn(true);
        Mockito.when(parent.getFunction("test/foo")).thenReturn(Mockito.mock(LPCFunction.class));

        LocalExecutionContext child = new LocalExecutionContext(parent);
        assertThat(child.hasFunction("test/foo")).isTrue();
        assertThat(child.getFunction("test/foo")).isNotNull();
    }

    @Test
    void providesAllFunctionsFromParentContext() {
        Map<String, LPCFunction> parentFunctions = Map.of("test/foo", Mockito.mock(LPCFunction.class), "bar", Mockito.mock(LPCFunction.class));
        LPCExecutionContext parent = Mockito.mock(LPCExecutionContext.class);
        Mockito.when(parent.getFunctions()).thenReturn(parentFunctions);

        LocalExecutionContext child = new LocalExecutionContext(parent);
        assertThat(child.getFunctions()).containsKey("test/foo").containsKey("bar");
    }
}