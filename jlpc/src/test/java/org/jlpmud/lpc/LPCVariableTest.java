package org.jlpmud.lpc;

import org.jlpmud.lpc.error.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.withinPercentage;

@ExtendWith(MockitoExtension.class)
class LPCVariableTest {

    @Test
    void intVariableThatIsNotANumberThrowsError() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "NaN");
        assertThatThrownBy(variable::intValue)
                .isInstanceOf(LPCRunTimeException.class)
                .hasMessage("invalid integer expression (NaN)");
    }

    @Test
    void intVariableWithAValidIntReturnsIntValue() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "1001");
        assertThat(variable.intValue()).isEqualTo(1001);
    }

    @Test
    void assigningNonIntegerToIntegerResultsInError() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER);
        LPCVariable newValue = new LPCVariable(LPCVariableType.STRING, "hello");
        assertThatThrownBy(() -> variable.setValue(newValue))
                .isInstanceOf(AssignmentTypeMismatch.class)
                .hasMessage("type mismatch in assignment (INTEGER vs STRING)");
    }

    @Test
    void asIntForNonIntegerThrowsError() {
        assertThatThrownBy(() -> LPCVariable.asInt("NaN"))
                .isInstanceOf(InvalidDataTypeExpression.class)
                .hasMessage("invalid INTEGER expression (NaN)");
    }

    @Test
    void stringVariableWithAValidStringReturnsStringValue() {
        LPCVariable variable = new LPCVariable(LPCVariableType.STRING, "hello");
        assertThat(variable.stringValue()).isEqualTo("hello");
    }

    @Test
    void intValueCanBeTakenOverFromAnotherIntVariable() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "1001");
        LPCVariable var2 = new LPCVariable(LPCVariableType.INTEGER);
        var2.setValue(variable);
        assertThat(var2.intValue()).isEqualTo(1001);
    }

    @Test
    void stringValueCanBeTakenOverFromAnotherStringVariable() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable var2 = new LPCVariable(LPCVariableType.STRING);
        var2.setValue(variable);
        assertThat(var2.stringValue()).isEqualTo("hello");
    }

    @Test
    void takingOverStringValueFromIntVariableFails() {
        LPCVariable intVar = new LPCVariable(LPCVariableType.INTEGER, "1001");
        LPCVariable stringVar = new LPCVariable(LPCVariableType.STRING);
        assertThatThrownBy(() -> stringVar.setValue(intVar))
                .isInstanceOf(AssignmentTypeMismatch.class)
                .hasMessage("type mismatch in assignment (STRING vs INTEGER)");
    }

    @Test
    void takingOverIntValueFromStringVariableFails() {
        LPCVariable stringVar = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable intVar = new LPCVariable(LPCVariableType.INTEGER);
        assertThatThrownBy(() -> intVar.setValue(stringVar))
                .isInstanceOf(AssignmentTypeMismatch.class)
                .hasMessage("type mismatch in assignment (INTEGER vs STRING)");
    }

    @Test
    void uninitializedIntVariableIsEqualToZero() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, null);
        assertThat(variable.intValue()).isZero();
    }

    @Test
    void addTwoIntegerVariables() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "1001");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "1002");
        LPCVariable result = left.add(right);
        assertThat(result.intValue()).isEqualTo(2003);
    }

    @Test
    void subtractTwoIntegerVariables() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "1001");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "1002");
        LPCVariable result = left.subtract(right);
        assertThat(result.intValue()).isEqualTo(-1);
    }

    @Test
    void divideTwoIntegers() throws InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "33");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "11");
        assertThat(left.divideBy(right).intValue()).isEqualTo(3);
    }

    @Test
    void cannotDivideString() {
        LPCVariable left = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable right = new LPCVariable(LPCVariableType.STRING, "world");
        assertThatThrownBy(() -> left.divideBy(right))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: division for type STRING");
    }

    @Test
    void multiplyTwoIntegers() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "33");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "11");
        assertThat(left.multiply(right).intValue()).isEqualTo(363);
    }

    @Test
    void cannotMultiplyString() {
        LPCVariable left = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable right = new LPCVariable(LPCVariableType.STRING, "world");
        assertThatThrownBy(() -> left.multiply(right))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: multiplication for type STRING");
    }

    @Test
    void cannotSubtractFromString() {
        LPCVariable left = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable right = new LPCVariable(LPCVariableType.STRING, "world");
        assertThatThrownBy(() -> left.subtract(right))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: subtraction for type STRING");
    }

    @Test
    void intModuloInt() throws InvalidOperationException {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "7");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "5");
        assertThat(left.modulo(right).intValue()).isEqualTo(2);
    }

    @Test
    void cannotModuloString() {
        LPCVariable left = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable right = new LPCVariable(LPCVariableType.STRING, "world");
        assertThatThrownBy(() -> left.modulo(right))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: modulo for type STRING");
    }

    @Test
    void integerBitwiseAnd() {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "5");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "3");
        assertThat(left.and(right).intValue()).isEqualTo(1);
    }

    @Test
    void integerBitweiseOr() {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "5");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "3");
        assertThat(left.or(right).intValue()).isEqualTo(7);
    }

    @Test
    void integerBitweiseXor() {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "5");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "3");
        assertThat(left.xor(right).intValue()).isEqualTo(6);
    }

    @Test
    void integerBitweiseNot() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "5");
        assertThat(variable.not().intValue()).isEqualTo(-6);
    }

    @Test
    void integerBitwiseShiftLeft() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "5");
        assertThat(variable.shiftLeft(new BigInteger("1")).intValue()).isEqualTo(10);
    }

    @Test
    void integerBitwiseShiftRight() {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "5");
        assertThat(variable.shiftRight(new BigInteger("1")).intValue()).isEqualTo(2);
    }

    @Test
    void createSimpleFloatVariable() throws InvalidDataTypeExpression {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        assertThat(variable.floatValue()).isCloseTo(3.14f, withinPercentage(0.01));
    }

    @Test
    void invalidValueAsFloatThrowsError() {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT, "not a number");
        assertThatThrownBy(variable::floatValue)
                .isInstanceOf(InvalidDataTypeExpression.class)
                .hasMessage("invalid FLOAT expression (not a number)");
    }

    @Test
    void nullFloatVariableIsZero() throws InvalidDataTypeExpression {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT, null);
        assertThat(variable.floatValue()).isZero();
    }

    @Test
    void asFloatReturnsFloatVariableForValidDecimal() throws InvalidDataTypeExpression {
        assertThat(LPCVariable.asFloat("3.14").floatValue()).isCloseTo(3.14f, withinPercentage(0.01));
    }

    @Test
    void asFloatReturnsErrorForInvalidDecimal() {
        assertThatThrownBy(() -> LPCVariable.asFloat("xyz"))
                .isInstanceOf(LPCRunTimeException.class)
                .hasMessage("invalid float expression (xyz)");
    }

    @Test
    void addingFloatAndIntReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "2");
        LPCVariable result = left.add(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(result.floatValue()).isCloseTo(5.14f, withinPercentage(0.01));
    }

    @Test
    void addingIntegerAndFloatReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "2");
        LPCVariable right = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        LPCVariable result = left.add(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(result.intValue()).isEqualTo(new BigInteger("5"));
    }

    @Test
    void castFloatToInteger() throws InvalidCast {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        assertThat(variable.cast(LPCVariableType.INTEGER).intValue()).isEqualTo(3);
    }

    @Test
    void castIntegerToFloat() throws InvalidCast, InvalidDataTypeExpression {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "3");
        assertThat(variable.cast(LPCVariableType.FLOAT).floatValue()).isCloseTo(3.0f, withinPercentage(0.01));
    }

    @Test
    void subtractingFloatFromIntegerReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "5");
        LPCVariable right = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        LPCVariable result = left.subtract(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(result.floatValue()).isCloseTo(1.86f, withinPercentage(0.01));
    }

    @Test
    void subtractingIntegerFromFloatReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.FLOAT, "5.0");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "3");
        LPCVariable result = left.subtract(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(result.floatValue()).isCloseTo(2.0f, withinPercentage(0.01));
    }

    @Test
    void multiplyFloatWithIntReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.FLOAT, "7.5");
        LPCVariable right = new LPCVariable(LPCVariableType.INTEGER, "2");
        LPCVariable result = left.multiply(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(result.floatValue()).isCloseTo(15.0f, withinPercentage(0.01));
    }

    @Test
    void multiplyIntWithFloatReturnsFloat() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.INTEGER, "2");
        LPCVariable right = new LPCVariable(LPCVariableType.FLOAT, "7.6");
        LPCVariable result = left.multiply(right);
        assertThat(result.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(result.floatValue()).isCloseTo(15.2f, withinPercentage(0.01));
    }

    @Test
    void addStringToStringConcatenatesStrings() throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        LPCVariable left = new LPCVariable(LPCVariableType.STRING, "hello");
        LPCVariable right = new LPCVariable(LPCVariableType.STRING, "world");
        LPCVariable result = left.add(right);
        assertThat(result.stringValue()).isEqualTo("helloworld");
    }

    @Test
    void castIntToString() throws InvalidCast {
        LPCVariable variable = new LPCVariable(LPCVariableType.INTEGER, "42");
        assertThat(variable.cast(LPCVariableType.STRING).stringValue()).isEqualTo("42");
    }

    @Test
    void castFloatToString() throws InvalidCast {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT, "3.14");
        assertThat(variable.cast(LPCVariableType.STRING).stringValue()).isEqualTo("3.14");
    }

    @Test
    void castStringToString() throws InvalidCast {
        LPCVariable variable = new LPCVariable(LPCVariableType.STRING, "hello");
        assertThat(variable.cast(LPCVariableType.STRING).stringValue()).isEqualTo("hello");
    }

    @Test
    void atStringReturnsCharacterIntForPositionInString() throws InvalidOperationException {
        LPCVariable variable = new LPCVariable(LPCVariableType.STRING, "hello");
        assertThat(variable.at(new BigInteger("0")).intValue()).isEqualTo(104);
        assertThat(variable.at(new BigInteger("1")).intValue()).isEqualTo(101);
        assertThat(variable.at(new BigInteger("2")).intValue()).isEqualTo(108);
        assertThat(variable.at(new BigInteger("3")).intValue()).isEqualTo(108);
        assertThat(variable.at(new BigInteger("4")).intValue()).isEqualTo(111);
    }

    @Test
    void accessingSingleCharacterInStringWithNegativeValueThrowsIllegalIndex() {
        LPCVariable variable = new LPCVariable(LPCVariableType.STRING, "hello");
        BigInteger index = new BigInteger("-1");
        assertThatThrownBy(() -> variable.at(index))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: Illegal index -1 for length 5");
    }

    @Test
    void statusVariableCanBezero() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.STATUS);
        variable.setValue(new LPCVariable(LPCVariableType.INTEGER, "0"));
        assertThat(variable.intValue()).isZero();
    }

    @Test
    void statusVariableCanBeOne() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.STATUS);
        variable.setValue(new LPCVariable(LPCVariableType.INTEGER, "1"));
        assertThat(variable.intValue()).isEqualTo(1);
    }

    @Test
    void statusCannotBeAnyOtherInteger() {
        LPCVariable v = new LPCVariable(LPCVariableType.STATUS);
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "2");
        assertThatThrownBy(() -> v.setValue(value))
                .isInstanceOf(LPCRunTimeException.class)
                .hasMessage("invalid status expression (2)");
    }

    @Test
    void statusCannotBeAString() {
        LPCVariable v = new LPCVariable(LPCVariableType.STATUS);
        LPCVariable value = new LPCVariable(LPCVariableType.STRING, "hello");
        assertThatThrownBy(() -> v.setValue(value))
                .isInstanceOf(AssignmentTypeMismatch.class)
                .hasMessage("type mismatch in assignment (STATUS vs STRING)");
    }

    @Test
    void statusCanNotBeAddedTo() {
        LPCVariable status = new LPCVariable(LPCVariableType.STATUS, "0");
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "1");
        assertThatThrownBy(() -> status.add(value))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: addition for type INTEGER and STATUS");
    }

    @Test
    void statusCannotBeSubtractedFrom() {
        LPCVariable status = new LPCVariable(LPCVariableType.STATUS, "1");
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "1");
        assertThatThrownBy(() -> status.subtract(value))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: subtraction for type STATUS");
    }

    @Test
    void statusCannotBeMultipliedWith() {
        LPCVariable status = new LPCVariable(LPCVariableType.STATUS, "1");
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "1");
        assertThatThrownBy(() -> status.multiply(value))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: multiplication for type STATUS");
    }

    @Test
    void statusCannotBeDividedBy() {
        LPCVariable status = new LPCVariable(LPCVariableType.STATUS, "1");
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "1");
        assertThatThrownBy(() -> status.divideBy(value))
                .isInstanceOf(InvalidOperationException.class)
                .hasMessage("unsupported operation: division for type STATUS");
    }

    @Test
    void intValueCanBeAssignedToMixedVariable() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.UNDEF);
        variable.setMixed(true);
        LPCVariable value = new LPCVariable(LPCVariableType.INTEGER, "42");
        variable.setValue(value);
        assertThat(variable.intValue()).isEqualTo(42);
        assertThat(variable.type()).isEqualTo(LPCVariableType.INTEGER);
    }

    @Test
    void stringValueCanBeAssignedToMixedVariable() throws AssignmentTypeMismatch {
        LPCVariable variable = new LPCVariable(LPCVariableType.UNDEF);
        variable.setMixed(true);
        LPCVariable value = new LPCVariable(LPCVariableType.STRING, "hello");
        variable.setValue(value);
        assertThat(variable.stringValue()).isEqualTo("hello");
        assertThat(variable.type()).isEqualTo(LPCVariableType.STRING);
    }
}