package org.jlpmud.lpc;

import com.org.jlpmud.LPCLexer;
import com.org.jlpmud.LPCParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.assertj.core.api.Assertions;
import org.jlpmud.lpc.error.*;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.AssertionsForClassTypes.withinPercentage;


public class InterpreterTest {

    @Test
    void definedFunctionsAreRegisteredWithLPCObject() {
        LPCObject obj = getLPCObjectForTest();

        assertThat(obj.getFunctions()).containsKey("simpleInt").containsKey("somePublicVoidFun").containsKey("privateVoidFun");
    }

    @Test
    void definedGlobalIntVariableWithoutAssignedValueIsRegistered() {
        LPCObject obj = getLPCObjectForTest();

        assertThat(obj.getGlobalVariables()).containsKey("value");
        assertThat(obj.getVariable("value").type()).isEqualTo(LPCVariableType.INTEGER);
    }

    @Test
    void definedGlobalIntVariableWithAssignedValueIsRegisteredWithValue() {
        LPCObject obj = getLPCObjectForTest();

        assertThat(obj.getGlobalVariables()).containsKey("valueA");
        LPCVariable variable = obj.getVariable("valueA");
        assertThat(variable).isNotNull();
        assertThat(variable.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(variable.intValue()).isEqualTo(42);

        assertThat(obj.getGlobalVariables()).containsKey("valueB");
        variable = obj.getVariable("valueB");
        assertThat(variable).isNotNull();
        assertThat(variable.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(variable.intValue()).isEqualTo(58);
    }

    @Test
    void intVariableCanBeReassignedAfterDefinition() {
        LPCObject obj = getLPCObjectForTest();

        assertThat(obj.getVariable("x").intValue()).isEqualTo(77);
    }

    @Test
    void basicIntegerArithmetics() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("multiplication").intValue()).isEqualTo(33);
        assertThat(obj.getVariable("addition").intValue()).isEqualTo(100);
        assertThat(obj.getVariable("division").intValue()).isEqualTo(11);
        assertThat(obj.getVariable("subtraction").intValue()).isEqualTo(1);
        assertThat(obj.getVariable("modulo").intValue()).isEqualTo(1);
        assertThat(obj.getVariable("complex").intValue()).isEqualTo(11);
    }

    @Test
    void globalIntegerVariableDefinitionWithANonIntegerValueThrowsError() {
        assertThatThrownBy(() -> getLPCObjectForTest("globalIntegerVariableDefinitionWithANonIntegerValueThrowsError"))
                .isInstanceOf(LPCRunTimeException.class)
                .hasMessage("type mismatch in assignment (INTEGER vs STRING)");
    }

    @Test
    void globalVariableAssignmentForIntIsEvaluated() {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable variable = obj.getVariable("evaluatedVariable");

        assertThat(variable).isNotNull();
        assertThat(variable.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(variable.intValue()).isEqualTo(99);
    }

    @Test
    void integerArithmeticsWithReferredVariables() {
        LPCObject obj = getLPCObjectForTest("integerArithmeticsWithReferredVariables");
        LPCVariable variable = obj.getVariable("evaluatedVariable");

        assertThat(variable).isNotNull();
        assertThat(variable.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(variable.intValue()).isEqualTo(99);
    }

    @Test
    void selfReferencedDefinitionOfVariableReturnsUnknownVariableError() {
        assertThatThrownBy(() -> getLPCObjectForTest("selfReferencedDefinitionOfVariableReturnsUnknownVariableError")).hasMessage("undefined variable: a").isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void executeCalledVoidFunction() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("fun").execute();
        assertThat(obj.getVariable("value").intValue()).isEqualTo(42); // function changed global value
    }

    @Test
    void voidFunctionInAssignmentResultsInZero() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("value").intValue()).isZero(); // void evaluates to 0
    }

    @Test
    void voidFunctionCalledInAssignmentStillExecutesSideEffects() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("globVar").intValue()).isEqualTo(1); // function changed global value
    }

    @Test
    void reassigningValueOfNotDefinedVariableYieldsError() {
        LPCRunTimeException e = catchThrowableOfType(() -> getLPCObjectForTest("reassigningValueOfNotDefinedVariableYieldsError"), LPCRunTimeException.class);
        assertThat(e).hasMessage(("undefined variable: x"));
        assertThat(e.getLine()).isEqualTo(1);
        assertThat(e.getPosition()).isEqualTo(0);
    }

    @Test
    void dividingStringsInFunctionIsNotPossible() {
        LPCObject obj = getLPCObjectForTest("dividingStringsInFunctionIsNotPossible");
        LPCFunction func = obj.getFunction("divideStrings");
        assertThat(func).isNotNull();

        var error = catchThrowableOfType(func::execute, LPCRunTimeException.class);
        assertThat(error).isInstanceOf(LPCRunTimeException.class);
        assertThat(error.getLine()).isEqualTo(2);
        assertThat(error.getPosition()).isEqualTo(17);
    }

    @Test
    void singleVariableCanBeDefinedInFunctionWithoutValue() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        LPCFunction func = obj.getFunction("singleVariableInFunction");
        func.execute();

        assertThat(obj.getVariable("returnValue")).isNotNull();
        assertThat(obj.getVariable("returnValue").type()).isEqualTo(LPCVariableType.INTEGER);
    }

    @Test
    void callVoidFunctionWithParametersForSideEffects() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();

        LPCFunction func = obj.getFunction("sumUp");
        Map<String, LPCVariable> params = Map.of("a", LPCVariable.asInt("1"), "b", LPCVariable.asInt("2"));
        func.execute(params);

        LPCVariable returnValue = obj.getVariable("returnValue");
        assertThat(returnValue).isNotNull();
        assertThat(returnValue.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(returnValue.intValue()).isEqualTo(3);
    }

    @Test
    void variableMustBeDeclaredBeforeFirstUse() {
        var error = catchThrowableOfType(() -> getLPCObjectForTest("variableMustBeDeclaredBeforeFirstUse"), LPCRunTimeException.class);
        assertThat(error).hasMessage("undefined variable: x");
        assertThat(error.getLine()).isEqualTo(1);
        assertThat(error.getPosition()).isEqualTo(0);
    }

    @Test
    void uninitializedParametersInFunctionCallsAreZero() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        LPCFunction func = obj.getFunction("sumUp");
        Map<String, LPCVariable> params = Map.of("a", LPCVariable.asInt("1"));
        func.execute(params);
        assertThat(obj.getVariable("returnValue").intValue()).isEqualTo(1);
    }

    @Test
    void integersCanBeLargerThanMaxLong() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("bigInt").intValue()).isEqualTo(new BigInteger("1234567890123456789012345678901234567890"));
    }

    @Test
    void multipleVariablesCanBeDeclaredGloballyInOneStatement() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("a")).isNotNull();
        assertThat(obj.getVariable("a").type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(obj.getVariable("b")).isNotNull();
        assertThat(obj.getVariable("b").type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(obj.getVariable("c")).isNotNull();
        assertThat(obj.getVariable("c").type()).isEqualTo(LPCVariableType.INTEGER);
    }

    @Test
    void multipleVariablesCanBeDeclaredInOneLineInLocalContext() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        LPCFunction func = obj.getFunction("multipleVariablesInFunction");
        func.execute();
        assertThat(obj.getVariable("globA").intValue()).isEqualTo(1);
        assertThat(obj.getVariable("globB").intValue()).isEqualTo(2);
        assertThat(obj.getVariable("globC").intValue()).isEqualTo(3);
    }

    @Test
    void operatorAssignmentCombinationsForInteger() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("add").intValue()).isEqualTo(100);
        assertThat(obj.getVariable("subtract").intValue()).isEqualTo(48);
        assertThat(obj.getVariable("multiply").intValue()).isEqualTo(33);
        assertThat(obj.getVariable("divide").intValue()).isEqualTo(5);
        assertThat(obj.getVariable("mod").intValue()).isEqualTo(2);
    }

    @Test
    void bitwiseOperationsForIntegerInFunction() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("bitwiseAnd").intValue()).isEqualTo(2);
        assertThat(obj.getVariable("bitwiseOr").intValue()).isEqualTo(3);
        assertThat(obj.getVariable("bitwiseXor").intValue()).isEqualTo(112);
        assertThat(obj.getVariable("bitwiseNot").intValue()).isEqualTo(-8);
        assertThat(obj.getVariable("bitwiseShiftLeft").intValue()).isEqualTo(12);
        assertThat(obj.getVariable("bitwiseShiftRight").intValue()).isEqualTo(9);
    }

    @Test
    void bitwiseOperationsCombinedWithAssignment() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("bitwiseAnd").intValue()).isEqualTo(2);
        assertThat(obj.getVariable("bitwiseOr").intValue()).isEqualTo(3);
        assertThat(obj.getVariable("bitwiseXor").intValue()).isEqualTo(112);
        assertThat(obj.getVariable("bitwiseShiftLeft").intValue()).isEqualTo(12);
        assertThat(obj.getVariable("bitwiseShiftRight").intValue()).isEqualTo(9);
    }

    @Test
    void basicFloatAssignment() throws InvalidDataTypeExpression {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("floatVar").floatValue()).isCloseTo(3.14f, withinPercentage(0.01));
    }

    @Test
    void basicFloatAssignmentFailsForInvalidFloat() {
        var error = catchThrowableOfType(() -> getLPCObjectForTest("basicFloatAssignmentFailsForInvalidFloat"), LPCRunTimeException.class);
        assertThat(error).hasMessage("type mismatch in assignment (FLOAT vs STRING)");
        assertThat(error.getLine()).isEqualTo(1);
        assertThat(error.getPosition()).isEqualTo(0);
    }

    @Test
    void floatCanBeDeclaredInFunction() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        LPCFunction func = obj.getFunction("floatInFunction");
        func.execute();
        assertThat(obj.getVariable("returnValue").floatValue()).isCloseTo(3.14f, withinPercentage(0.01));
    }

    @Test
    void declaringNonFloatFloatInFunctionFails() {
        LPCObject obj = getLPCObjectForTest();
        LPCFunction func = obj.getFunction("func");
        assertThatThrownBy(func::execute)
                .hasMessage("type mismatch in assignment (STRING vs FLOAT)")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void functionLocalDefinitionMustNotBleedIntoGlobalContext() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("localVar")).isNull();
    }

    @Test
    void floatCannotBeRedefinedAsIntInFunction() {
        LPCObject obj = getLPCObjectForTest("floatCannotBeRedefinedAsIntInFunction");
        LPCFunction func = obj.getFunction("func");
        assertThatThrownBy(func::execute)
                .hasMessage("variable already defined: localVar")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void floatingPointArithmetics() throws InvalidDataTypeExpression {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("fAdd").floatValue()).isCloseTo(0.2f, withinPercentage(0.01));
        assertThat(obj.getVariable("fSub").floatValue()).isCloseTo(0.4f, withinPercentage(0.01));
        assertThat(obj.getVariable("fMul").floatValue()).isCloseTo(1.0f, withinPercentage(0.01));
        assertThat(obj.getVariable("fDiv").floatValue()).isCloseTo(2.5f, withinPercentage(0.01));
        assertThat(obj.getVariable("complex").floatValue()).isCloseTo(0.6f, withinPercentage(0.01));
    }

    @Test
    void concatenateStringsWithPlus() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("s1").stringValue()).isEqualTo("ab");
        assertThat(obj.getVariable("s2").stringValue()).isEqualTo("Hello, 1");
        assertThat(obj.getVariable("s3").stringValue()).isEqualTo("7.5g of apples");
        assertThat(obj.getVariable("s4").stringValue()).isEqualTo("42 is the answer");
        assertThat(obj.getVariable("s5").stringValue()).isEqualTo("No, it is 4.2");
    }

    @Test
    void assignSimpleString() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("str").stringValue()).isEqualTo("Hello, World!");
    }

    @Test
    void assignStringsWithEscapeChars() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("quote").stringValue()).isEqualTo("Hello, \"World\"!");
        assertThat(obj.getVariable("newline").stringValue()).isEqualTo("Hello,\nWorld!");
        assertThat(obj.getVariable("tab").stringValue()).isEqualTo("Hello,\tWorld!");
        assertThat(obj.getVariable("backslash").stringValue()).isEqualTo("Hello, \\World!");
        assertThat(obj.getVariable("carriageReturn").stringValue()).isEqualTo("Hello,\rWorld!");
        assertThat(obj.getVariable("questionMark").stringValue()).isEqualTo("Hello, World?");
        assertThat(obj.getVariable("apostrophe").stringValue()).isEqualTo("Hello, 'World'!");
    }

    @Test
    void bracketOperatorCanBeUsedToAccessIndividualCharactersInString() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("s").stringValue()).isEqualTo("abcd");
        assertThat(obj.getVariable("first").intValue()).isEqualTo(97);
        assertThat(obj.getVariable("second").intValue()).isEqualTo(98);
        assertThat(obj.getVariable("third").intValue()).isEqualTo(99);
        assertThat(obj.getVariable("fourth").intValue()).isEqualTo(100);
    }

    @Test
    void characterAccessInStringWithNegativeValueFails() {
        assertThatThrownBy(() -> getLPCObjectForTest("characterAccessInStringWithNegativeValueFails"))
                .hasMessage("unsupported operation: Illegal index 1 for length 1")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void characterAccessInStringWithPositionOverLengthFails() {
        assertThatThrownBy(() -> getLPCObjectForTest("characterAccessInStringWithPositionOverLengthFails"))
                .hasMessage("unsupported operation: Illegal index 5 for length 4")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void assignIntToStringResultsInError() {
        assertThatThrownBy(() -> getLPCObjectForTest("assignIntToStringResultsInError"))
                .hasMessage("type mismatch in assignment (STRING vs INTEGER)")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void statusCanBeZeroOrOne() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("a").stringValue()).isEqualTo("1");
        assertThat(obj.getVariable("b").stringValue()).isEqualTo("0");
    }

    @Test
    void statusCannotBeAnotherInteger() {
        assertThatThrownBy(() -> getLPCObjectForTest("statusCannotBeAnotherInteger"))
                .hasMessage("invalid status expression (47)")
                .isInstanceOf(LPCRunTimeException.class);
    }

    @Test
    void addAssignConcatsStrings() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("s").stringValue()).isEqualTo("Hello, World!");
    }

    @Test
    void minusAssignOperatorOnFloatAndIntDecreasesByGivenNumber() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("f").floatValue()).isCloseTo(3.7f, withinPercentage(0.01));
        assertThat(obj.getVariable("i").intValue()).isEqualTo(5);
    }

    @Test
    void addAssignOperatorOnFloatAndIntIncreasesByGiveNUmber() throws InvalidDataTypeExpression, AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("f").floatValue()).isCloseTo(4.3f, withinPercentage(0.01));
        assertThat(obj.getVariable("i").intValue()).isEqualTo(15);
    }

    @Test
    void minusMinusOperatorDecreasesValueByOne() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("i").intValue()).isEqualTo(1);
        assertThat(obj.getVariable("j").intValue()).isEqualTo(3);
        assertThat(obj.getVariable("k").intValue()).isEqualTo(1);
    }

    @Test
    void plusPlusOperatorIncreasesValueByOne() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("i").intValue()).isEqualTo(5);
        assertThat(obj.getVariable("j").intValue()).isEqualTo(3);
        assertThat(obj.getVariable("k").intValue()).isEqualTo(5);
    }

    @Test
    void useMixedDataTypeToRepresentAnInteger() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("i").intValue()).isEqualTo(42);
        assertThat(obj.getVariable("j").intValue()).isEqualTo(43);
    }

    @Test
    void useMixedDataTypeToRepresentAString() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("func").execute();
        assertThat(obj.getVariable("s").stringValue()).isEqualTo("Hello, World!");
    }

    @Test
    void useMixedDataTypeToRepresentAFloat() throws InvalidDataTypeExpression {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("f").floatValue()).isCloseTo(3.14f, withinPercentage(0.01));
    }

    @Test
    void useMixedDataTypeToRepresentABoolean() {
        LPCObject obj = getLPCObjectForTest();
        assertThat(obj.getVariable("b").stringValue()).isEqualTo("1");
    }

    @Test
    void callAStringFunctionReturnsAString() {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable var = obj.getVariable("caption");
        assertThat(var.type()).isEqualTo(LPCVariableType.STRING);
        assertThat(var.stringValue()).isEqualTo("str returned from function");
    }

    @Test
    void callAIntFunctionReturnsAInt() {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable var = obj.getVariable("intValue");
        assertThat(var.type()).isEqualTo(LPCVariableType.INTEGER);
        assertThat(var.intValue()).isEqualTo(BigInteger.valueOf(47));
    }

    @Test
    void callAFloatFunctionReturnsAFloat() throws InvalidDataTypeExpression {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable var = obj.getVariable("fValue");
        assertThat(var.type()).isEqualTo(LPCVariableType.FLOAT);
        assertThat(var.floatValue()).isEqualTo(197.4443);
    }

    @Test
    void aFunctionCanCallAnotherFunction() {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable var = obj.getVariable("str");
        assertThat(var.stringValue()).isEqualTo("Hello, World!");
    }

    @Test
    void canPassParametersToFunction() {
        LPCObject obj = getLPCObjectForTest();
        LPCVariable var = obj.getVariable("str");
        assertThat(var.stringValue()).isEqualTo("I greet you 5 times friendly.");
    }

    @Test
    void mismatchBetweenDeclaredFunctionParameterAndPassedVariableIsAnError() {
        assertThatThrownBy(() -> getLPCObjectForTest("mismatchBetweenDeclaredFunctionParameterAndPassedVariableIsAnError"))
                .isInstanceOf(LPCMismatchedFunctionParameterException.class)
                .hasMessage("mismatched function parameter 'a': STRING passed for INTEGER expected");
    }

    @Test
    void mismatchedReturnedTypeInFunctionIsAnError() {
        var error = catchThrowableOfType(() -> getLPCObjectForTest("mismatchedReturnedTypeInFunctionIsAnError"), LPCRunTimeException.class);
        assertThat(error).hasMessage("mismatched return type: INTEGER returned for STRING");
        assertThat(error.getLine()).isEqualTo(5);
        assertThat(error.getPosition()).isEqualTo(13);
    }

    @Test
    void callingReturnInFunctionEndsExecution() throws AssignmentTypeMismatch {
        LPCObject obj = getLPCObjectForTest();
        obj.getFunction("fn").execute();
        assertThat(obj.getVariable("str").stringValue()).isEqualTo("Hello, World!");
    }


    /** Harness code starts here **/

    private LPCObject getLPCObjectForTest(String filename) {
        LPCParser parser = parseCodeForTest(filename);
        LPCObject obj = new LPCObject();
        parser.setErrorHandler(new BailErrorStrategy());
        try {
            ParseTreeWalker.DEFAULT.walk(new LPCProgramInterpreter(obj), parser.lpc_program());
        } catch(ParseCancellationException e) {
            String error = "Parsing of test code snipped '"+filename+".c' failed";
            if(e.getCause() instanceof InputMismatchException castEx) {
                error += " (line: " +castEx.getOffendingToken().getLine()+" position: "+castEx.getOffendingToken().getCharPositionInLine()+")";
            }
            Assertions.fail(error);
        }
        assertThat(obj).isNotNull();
        return obj;
    }

    private LPCObject getLPCObjectForTest() {
        String methodName = getCallingMethodName();
        return getLPCObjectForTest(methodName);
    }

    /*
        This is some boilerplate to always load test code from a file called lpc/<testname>.c
        and run it through the LPCParser. This makes it easier for us to test all the interpreter
        functionality.
     */
    public static LPCParser parseCodeForTest(String methodName) {
        String filePath = "/lpc/" + methodName + ".c";
        InputStream inputStream = InterpreterTest.class.getResourceAsStream(filePath);

        if (inputStream == null) {
            throw new RuntimeException("File not found: " + filePath);
        }

        try {
            Lexer lexer = new LPCLexer(CharStreams.fromStream(inputStream));
            TokenStream tokenStream = new CommonTokenStream(lexer);
            return new LPCParser(tokenStream);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load file: " + filePath, e);
        }
    }

    private static String getCallingMethodName() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        // StackTrace[0] is getStackTrace, [1] is getCallingMethodName, [2] is loadFileBasedOnMethodName, [3] is the caller method
        return stackTrace[3].getMethodName();
    }
}
