package org.jlpmud.lpc;

import org.jlpmud.lpc.error.ParserFailedException;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

class LPCInterpreterTest {

    @Test
    void interpretProgramWithValidCode() throws ParserFailedException {
        String validLpcCode = "int myFunction() { return 1; }";

        var interpreter = new LPCInterpreter();
        LPCObject result = interpreter.interpretProgramAsObject(validLpcCode);

        assertThat(result).isNotNull();
        assertThat(result.getFunctions()).containsKey("myFunction");
    }

    @Test
    void parserFailsOnInvalidCodeCompilation() {
        String invalidCode = "return /";
        var interpreter = new LPCInterpreter();
        var error = catchThrowableOfType(() -> interpreter.interpretProgramAsObject(invalidCode), ParserFailedException.class);
        assertThat(error).hasMessage("line 1:0: mismatched input 'return' expecting {<EOF>, '*', TypeModifier, BasicType, 'class', ':', 'foo', 'inherit', Identifier}");
    }

}
