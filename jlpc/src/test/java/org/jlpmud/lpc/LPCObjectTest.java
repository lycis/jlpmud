package org.jlpmud.lpc;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

class LPCObjectTest {
    @Test
    void hasFunctionTrueWhenFunctionPreviouslyAdded() {
        LPCObject obj = new LPCObject();
        obj.getFunctions().put("someFunction", Mockito.mock(LPCFunction.class));
        assertThat(obj.hasFunction("someFunction")).isTrue();
    }

    @Test
    void hasFunctionFalseWhenFunctionNotAdded() {
        LPCObject obj = new LPCObject();
        assertThat(obj.hasFunction("someFunction")).isFalse();
    }

    @Test
    void getFunctionReturnsFunctionWhenFunctionPreviouslyAdded() {
        LPCObject obj = new LPCObject();
        LPCFunction function = Mockito.mock(LPCFunction.class);
        obj.getFunctions().put("someFunction", function);
        assertThat(obj.getFunction("someFunction")).isEqualTo(function);
    }

    @Test
    void getFunctionReturnsNullWhenFunctionNotAdded() {
        LPCObject obj = new LPCObject();
        assertThat(obj.getFunction("someFunction")).isNull();
    }

    @Test
    void setVariableAddsVariableToGlobalVariables() {
        LPCObject obj = new LPCObject();
        LPCVariable variable = Mockito.mock(LPCVariable.class);
        obj.setVariable("someVariable", variable);
        assertThat(obj.getGlobalVariables()).containsEntry("someVariable", variable);
    }

    @Test
    void getVariableReturnsVariableWhenVariablePreviouslyAdded() {
        LPCObject obj = new LPCObject();
        LPCVariable variable = Mockito.mock(LPCVariable.class);
        obj.getGlobalVariables().put("someVariable", variable);
        assertThat(obj.getVariable("someVariable")).isEqualTo(variable);
    }

    @Test
    void getVariableReturnsNullWhenVariableNotAdded() {
        LPCObject obj = new LPCObject();
        assertThat(obj.getVariable("someVariable")).isNull();
    }
}