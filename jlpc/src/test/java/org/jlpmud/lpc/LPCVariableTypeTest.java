package org.jlpmud.lpc;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LPCVariableTypeTest {

    @Test
    void intFromStringReturnsINTEGER() {
        assertThat(LPCVariableType.fromString("int")).isEqualTo(LPCVariableType.INTEGER);
    }

    @Test
    void stringFromStringReturnsSTRING() {
        assertThat(LPCVariableType.fromString("string")).isEqualTo(LPCVariableType.STRING);
    }

    @Test
    void undefFromStringReturnsUNDEF() {
        assertThat(LPCVariableType.fromString("##undefined##")).isEqualTo(LPCVariableType.UNDEF);
    }

    @Test
    void floatFromStringReturnsFloat() {
        assertThat(LPCVariableType.fromString("float")).isEqualTo(LPCVariableType.FLOAT);
    }
}