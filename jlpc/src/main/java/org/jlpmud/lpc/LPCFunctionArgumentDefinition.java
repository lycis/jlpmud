package org.jlpmud.lpc;

// represents metadata about a function argument
public record LPCFunctionArgumentDefinition(String name, LPCVariableType type) {
    public String toString() {
        return type + " " + name;
    }
}
