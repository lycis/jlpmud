package org.jlpmud.lpc;

import org.jlpmud.lpc.error.AssignmentTypeMismatch;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a specialized execution context that can be nested within other
 * execution contexts (e.g. objects or other LocalExecutionContexts). It
 * retains a localized set of variables (no functions!) and can read/write
 * (but not create new) variables in the parent context.
 * This represents a localized code block (e.g. a function body) or inside
 * an if/else block.
 */
public class LocalExecutionContext implements LPCExecutionContext {

    private final LPCExecutionContext parent;
    private final Map<String, LPCVariable> localVariables;

    public LocalExecutionContext(LPCExecutionContext parent) {
        this.parent = parent;
        this.localVariables = new HashMap<>();
    }

    @Override
    public LPCVariable getVariable(String name) {
        if(localVariables.containsKey(name)) {
            return localVariables.get(name);
        }

        return parent.getVariable(name);
    }

    @Override
    public void setVariable(String name, LPCVariable variable) throws AssignmentTypeMismatch {
        if(localVariables.containsKey(name)) {
            localVariables.get(name).setValue(variable);
        } else {
            if(parent.getVariable(name) == null) {
                localVariables.put(name, variable);
            } else {
                parent.getVariable(name).setValue(variable);
            }
        }
    }

    @Override
    public boolean hasFunction(String functionName) {
        return parent.hasFunction(functionName);
    }

    @Override
    public LPCFunction getFunction(String functionName) {
        return parent.getFunction(functionName);
    }

    @Override
    public Map<String, LPCFunction> getFunctions() {
        return parent.getFunctions();
    }
}
