package org.jlpmud.lpc;

import com.org.jlpmud.LPCParser;
import org.jlpmud.lpc.error.AssignmentTypeMismatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LPCFunction {
    private final LPCParser.BlockContext content;
    private LPCExecutionContext ctx;
    private final List<LPCFunctionArgumentDefinition> arguments = new ArrayList<>();
    private final LPCVariableType returnType;

    public LPCFunction(LPCParser.Function_definationContext funDef) {
        LPCParser.Block_or_semiContext blsCtx;
        if(!(funDef.getChild(6) instanceof LPCParser.Block_or_semiContext)) {
            throw new IllegalArgumentException("Function definition does not contain a block");
        }

        if(funDef.argument() != null) {
            parseAndStoreArgumentDefinition(funDef.argument().argument_list());
        }

        blsCtx = (LPCParser.Block_or_semiContext) funDef.getChild(6);
        if(blsCtx.block() == null) {
            throw new IllegalArgumentException("Function definition does not contain a block");
        }

        returnType = LPCVariableType.fromString(funDef.data_type().getText());

        this.content = blsCtx.block();
        this.ctx = null;
    }

    private void parseAndStoreArgumentDefinition(LPCParser.Argument_listContext argument) {
        if(argument == null) return;

        LPCVariableType dtype = LPCVariableType.fromString(argument.new_arg().basic_type().getText());
        String name = argument.new_arg().new_local_name().Identifier().getText();
        arguments.add(new LPCFunctionArgumentDefinition(name, dtype));
        parseAndStoreArgumentDefinition(argument.argument_list());
    }

    public LPCFunction(LPCExecutionContext ctx, LPCParser.Function_definationContext funDef) {
       this(funDef);
       this.ctx = ctx;
    }

    public void execute() throws AssignmentTypeMismatch {
        execute(Map.of());
    }

    public LPCVariable execute(Map<String, LPCVariable> params) throws AssignmentTypeMismatch {
        LocalExecutionContext localCtx = new LocalExecutionContext(ctx);
        initializeFunctionArgumentsInContext(localCtx);
        for(var entry : params.entrySet()) {
            localCtx.setVariable(entry.getKey(), entry.getValue());
        }

        LPCBlockInterpreter interpreter = new LPCBlockInterpreter(localCtx);
        interpreter.setExpectedReturnType(returnType);
        interpreter.visit(content);
        return interpreter.getReturnValue();
    }

    private void initializeFunctionArgumentsInContext(LocalExecutionContext localCtx) throws AssignmentTypeMismatch {
        for(LPCFunctionArgumentDefinition arg : arguments) {
            localCtx.setVariable(arg.name(), new LPCVariable(arg.type(), null));
        }
    }

    public String toString() {
        return "LPCFunction{" +
                "content='...'" +
                ", arguments=" + arguments +
                '}';
    }

    public List<LPCFunctionArgumentDefinition> getArguments() {
        return arguments;
    }
}
