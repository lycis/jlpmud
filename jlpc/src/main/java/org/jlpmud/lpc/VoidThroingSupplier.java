package org.jlpmud.lpc;

@FunctionalInterface
public interface VoidThroingSupplier {
    void get() throws Exception;
}