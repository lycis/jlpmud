package org.jlpmud.lpc;

public enum LPCVariableType {
    INTEGER, STRING, UNDEF, FLOAT, STATUS, MIXED;

    public static LPCVariableType fromString(String dataType) {
        return switch (dataType) {
            case "int" -> INTEGER;
            case "string" -> STRING;
            case "float" -> FLOAT;
            case "status" -> STATUS;
            case "mixed" -> MIXED;
            default -> UNDEF;
        };
    }
}
