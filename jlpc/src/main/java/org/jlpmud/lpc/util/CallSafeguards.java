package org.jlpmud.lpc.util;

import org.antlr.v4.runtime.ParserRuleContext;
import org.jlpmud.lpc.ThrowingSupplier;
import org.jlpmud.lpc.VoidThroingSupplier;
import org.jlpmud.lpc.error.LPCRunTimeException;
import org.jlpmud.lpc.error.TokenErrorLocation;

public class CallSafeguards {

    public static <T> T returningSafeCall(ThrowingSupplier<T> function, ParserRuleContext ctx) throws LPCRunTimeException {
        try {
            return function.get();
        } catch (Exception e) {
            throw new LPCRunTimeException(e.getMessage(), new TokenErrorLocation(ctx.getStart()));
        }
    }

    public static void safeCall(VoidThroingSupplier f, ParserRuleContext ctx) {
        try {
            f.get();
        } catch (Exception e) {
            throw new LPCRunTimeException(e.getMessage(), new TokenErrorLocation(ctx.getStart()));
        }
    }
}
