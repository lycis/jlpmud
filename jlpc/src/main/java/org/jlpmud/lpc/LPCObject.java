package org.jlpmud.lpc;

import java.util.HashMap;
import java.util.Map;

public class LPCObject implements LPCExecutionContext {
    private final HashMap<String, LPCFunction> functions = new HashMap<>();
    private final HashMap<String, LPCVariable> globalVariables = new HashMap<>();


    public Map<String, LPCFunction> getFunctions() {
        return functions;
    }

    public Map<String, LPCVariable> getGlobalVariables() {
        return globalVariables;
    }

    public LPCVariable getVariable(String variableName) {
        return globalVariables.get(variableName);
    }

    @Override
    public boolean hasFunction(String functionName) {
        return functions.containsKey(functionName);
    }

    @Override
    public LPCFunction getFunction(String functionName) {
        return functions.get(functionName);
    }

    public void setVariable(String name, LPCVariable variable) {
        globalVariables.put(name, variable);
    }

    public String toString() {
        return "LPCObject{" +
                "functions=" + functions +
                ", globalVariables=" + globalVariables +
                '}';
    }
}
