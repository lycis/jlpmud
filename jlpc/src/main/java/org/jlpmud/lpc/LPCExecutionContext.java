package org.jlpmud.lpc;

import org.jlpmud.lpc.error.AssignmentTypeMismatch;

import java.util.Map;

// The execution context represents an interface for the part of the code that
// is "housing" another part of code. It is used when e.g. interpreting an expression
// or a variable and provides access to all defined variables and functions in a specific
// scope of the code.
public interface LPCExecutionContext {

     // Returns a variable from the context if defined or null
    LPCVariable getVariable(String name);

    // set a variable in the context
    void setVariable(String name, LPCVariable variable) throws AssignmentTypeMismatch;

    // returns true if the function is available in the context
    boolean hasFunction(String functionName);

    // returns the function if available or null
    LPCFunction getFunction(String functionName);
    // returns all known functions
    Map<String, LPCFunction> getFunctions();
}
