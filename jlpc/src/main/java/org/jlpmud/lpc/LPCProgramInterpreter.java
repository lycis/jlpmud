package org.jlpmud.lpc;

import com.org.jlpmud.LPCBaseListener;
import com.org.jlpmud.LPCParser;
import org.jlpmud.lpc.error.LPCRunTimeException;
import org.jlpmud.lpc.error.TokenErrorLocation;

import static org.jlpmud.lpc.util.CallSafeguards.safeCall;

public class LPCProgramInterpreter extends LPCBaseListener {

    private final LPCExecutionContext executionContext;

    public LPCProgramInterpreter(LPCExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    @Override
    public void enterFunction_defination(LPCParser.Function_definationContext ctx) {
        LPCFunction fun = new LPCFunction(executionContext, ctx);
        executionContext.getFunctions().put(ctx.identifier().getText(), fun);
    }

    @Override
    public void enterGlobal_variable_definition(LPCParser.Global_variable_definitionContext ctx) {
        if(LPCVariableType.fromString(ctx.data_type().getText()) == LPCVariableType.UNDEF) {
           reassignGlobalVariable(ctx);
        } else if(ctx.name_list() != null && ctx.name_list().new_name() != null && ctx.name_list().new_name().Assign() != null) {
            // The element contains a Assign token and thereby includes a value
            declareAndAssignVariable(ctx);
        } else {
            declareVariable(LPCVariableType.fromString(ctx.data_type().getText()), ctx.name_list());
        }
    }

    private LPCVariable declareVariable(LPCVariableType dType, LPCParser.Name_listContext ctx) {
        String vName;
        LPCVariable variable = new LPCVariable(dType);
        vName = ctx.new_name().identifier().getText();
        safeCall( () -> executionContext.setVariable(vName, variable), ctx);
        if(ctx.name_list() != null) {
            declareVariable(dType, ctx.name_list());
        }
        return variable;
    }

    private void declareAndAssignVariable(LPCParser.Global_variable_definitionContext ctx) {
        LPCVariable tmpVar = evaluateVariableAssignment(ctx);
        LPCVariableType dType = LPCVariableType.fromString(ctx.data_type().getText());
        LPCVariable variable = declareVariable(dType, ctx.name_list());

        if(dType == LPCVariableType.MIXED) {
            variable.setMixed(true);
        }

        safeCall( () -> variable.setValue(tmpVar), ctx);
    }

    private void assignVariable(LPCParser.Global_variable_definitionContext ctx, LPCVariable variable) {
        LPCVariable v = evaluateVariableAssignment(ctx);
        safeCall( () -> variable.setValue(v), ctx);
    }

    private LPCVariable evaluateVariableAssignment(LPCParser.Global_variable_definitionContext ctx) {
        LPCParser.New_nameContext varContext =  ctx.name_list().new_name();
        LPCBlockInterpreter interpreter = new LPCBlockInterpreter(executionContext);
        return interpreter.visit(varContext.expr0());
    }

    private void reassignGlobalVariable(LPCParser.Global_variable_definitionContext ctx) {
        LPCVariable variable;
        String vName;
        // datatype was not given -> this is a  reassignment of an existing variable on global level
        vName = ctx.name_list().new_name().identifier().getText();
        variable = executionContext.getVariable(vName);
        if(variable == null) {
            throw new LPCRunTimeException("undefined variable: "+ vName, new TokenErrorLocation(ctx.getStart()));
        }

        assignVariable(ctx, variable);
    }
}
