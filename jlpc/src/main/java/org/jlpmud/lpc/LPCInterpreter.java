package org.jlpmud.lpc;

import com.org.jlpmud.LPCLexer;
import com.org.jlpmud.LPCParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jlpmud.lpc.error.LPCInterpreterRuntimeException;
import org.jlpmud.lpc.error.ParserFailedException;

import java.util.BitSet;

public class LPCInterpreter implements ANTLRErrorListener {

    public LPCInterpreter() {
    }

    private LPCParser getParserFor(String code) {
        try {
            Lexer lexer = new LPCLexer(CharStreams.fromString(code));
            TokenStream tokenStream = new CommonTokenStream(lexer);
            var parser = new LPCParser(tokenStream);
            parser.addErrorListener(this);
            return parser;
        } catch (Exception e) {
            throw new LPCInterpreterRuntimeException("Failed to load code: " + code, e);
        }
    }

    public LPCObject interpretProgramAsObject(String code) throws ParserFailedException {
        LPCParser parser = getParserFor(code);
        LPCObject obj = new LPCObject();
        try {
            ParseTreeWalker.DEFAULT.walk(new LPCProgramInterpreter(obj), parser.lpc_program());
        } catch (ParseCancellationException e) {
            var error = "invalid code";
            if(e.getCause() instanceof InputMismatchException detailedError) {
                var offendingToken = detailedError.getOffendingToken();
                error += "at " + offendingToken.getLine() + " position " + offendingToken.getCharPositionInLine();
            }
            throw new ParserFailedException(error);
        }
        return obj;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object symbok, int line, int charPosition, String msg, RecognitionException e) {
        throw new ParserFailedException("line "+line+":"+charPosition+": "+msg);
    }

    @Override
    public void reportAmbiguity(Parser parser, DFA dfa, int i, int i1, boolean b, BitSet bitSet, ATNConfigSet atnConfigSet) {

    }

    @Override
    public void reportAttemptingFullContext(Parser parser, DFA dfa, int i, int i1, BitSet bitSet, ATNConfigSet atnConfigSet) {

    }

    @Override
    public void reportContextSensitivity(Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atnConfigSet) {

    }
}
