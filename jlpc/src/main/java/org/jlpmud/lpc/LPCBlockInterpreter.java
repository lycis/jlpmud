package org.jlpmud.lpc;

import com.org.jlpmud.LPCBaseVisitor;
import com.org.jlpmud.LPCParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.RuleNode;
import org.apache.commons.text.StringEscapeUtils;
import org.jlpmud.lpc.error.*;

import java.util.*;

import static org.jlpmud.lpc.util.CallSafeguards.returningSafeCall;
import static org.jlpmud.lpc.util.CallSafeguards.safeCall;

public class LPCBlockInterpreter extends LPCBaseVisitor<LPCVariable> {
    private final LPCExecutionContext executionContext;
    private LPCVariable returnValue;
    private LPCVariableType expectedReturnType;
    private boolean continueParsing = true;

    public LPCBlockInterpreter(LPCExecutionContext ctx) {
        this.executionContext = ctx;
        expectedReturnType = LPCVariableType.UNDEF;
    }

    @Override
    public LPCVariable visitFunction_call(LPCParser.Function_callContext ctx) {
        return super.visitFunction_call(ctx);
    }

    public void setExpectedReturnType(LPCVariableType expectedReturnType) {
        this.expectedReturnType = expectedReturnType;
    }


    @Override
    public LPCVariable visitAssignmentExpression(LPCParser.AssignmentExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);

        LPCVariable value = visit(ctx.expr0());

        // type mismatch error
        if(lpcVariable.type() != value.type()) {
            throw new LPCRunTimeException("type mismatch in assignment (" + lpcVariable.type().name() + " vs " + value.type().name()+")", new TokenErrorLocation(ctx.getStart()));
        }

        safeCall( () -> lpcVariable.setValue(value), ctx);
        return lpcVariable;
    }

    @Override
    public LPCVariable visitNumberExpression(LPCParser.NumberExpressionContext ctx) {
        return returningSafeCall( () -> LPCVariable.asInt(ctx.getText()), ctx);
    }

    @Override
    public LPCVariable visitRealExpression(LPCParser.RealExpressionContext ctx) {
        return LPCVariable.asFloat(ctx.getText());
    }

    @Override
    public LPCVariable visitAdditiveExpression(LPCParser.AdditiveExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return returningSafeCall(() -> left.add(right), ctx);
    }

    @Override
    public LPCVariable visitExpr4Identifier(LPCParser.Expr4IdentifierContext ctx) {
        return getVariableAssignToTarget(ctx.Identifier().getText(), ctx);
    }

    @Override
    public LPCVariable visitExpr4String(LPCParser.Expr4StringContext ctx) {
        String strValue = ctx.string().getText();
        strValue = strValue.substring(1, strValue.length()-1);
        strValue = StringEscapeUtils.unescapeJava(strValue);
        return new LPCVariable(LPCVariableType.STRING, strValue);
    }

    @Override
    public LPCVariable visitMultiplicativeExpression(LPCParser.MultiplicativeExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return returningSafeCall(() -> left.multiply(right), ctx);
    }

    @Override
    public LPCVariable visitExpr4ParenthesisList(LPCParser.Expr4ParenthesisListContext ctx) {
        // this is one of the strangest things in the grammar... but that what it should do (tested in Silberland)
        LPCParser.Comma_exprContext commaExpr = ctx.comma_expr();

        // execute all comma expressions in the list
        if(commaExpr.comma_expr() != null) {
            visit(commaExpr.comma_expr());
        }

        // but only return the last one
        return visit(ctx.comma_expr().expr0());
    }

    @Override
    public LPCVariable visitDivisionExpression(LPCParser.DivisionExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return returningSafeCall(() -> left.divideBy(right), ctx);
    }

    @Override
    public LPCVariable visitSubtractiveExpression(LPCParser.SubtractiveExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return returningSafeCall(() -> left.subtract(right), ctx);
    }

    @Override
    public LPCVariable visitModulusExpression(LPCParser.ModulusExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return returningSafeCall(() -> left.modulo(right), ctx);
    }

    @Override
    public LPCVariable visitFunction_name_call(LPCParser.Function_name_callContext ctx) {
        String funName = ctx.function_name().getText();
        if(!executionContext.hasFunction(funName)) {
            throw new LPCRunTimeException("undefined function: "+funName, new TokenErrorLocation(ctx.getStart()));
        }

        LPCFunction fun = executionContext.getFunction(funName);

        Map<String, LPCVariable> mappedParameters = mapPassedParametersToFunctionArgs(ctx, fun);

        LPCVariable returnValue =  returningSafeCall(() -> fun.execute(mappedParameters), ctx);

        // void functions return null and are equal to 0
        return Objects.requireNonNullElseGet(returnValue, () -> new LPCVariable(LPCVariableType.INTEGER, "0"));

    }

    private Map<String, LPCVariable> mapPassedParametersToFunctionArgs(LPCParser.Function_name_callContext ctx, LPCFunction fun) {
        List<LPCVariable> parameters = new ArrayList<>();
        if(ctx.expr_list() != null) {
            parameters = parametersToParameterList(ctx.expr_list());
        }

        Map<String, LPCVariable> mappedParameters = new HashMap<>();
        var arguments = fun.getArguments();
        for(int i=0; i< arguments.size(); ++i) {
            LPCFunctionArgumentDefinition expectedArgument = arguments.get(i);
            LPCVariable passedParameter = parameters.get(i);

            if(expectedArgument.type() != passedParameter.type()) {
                throw new LPCMismatchedFunctionParameterException(expectedArgument, passedParameter.type(), new TokenErrorLocation(ctx.getStart()));
            }

            mappedParameters.put(expectedArgument.name(), passedParameter);
        }
        return mappedParameters;
    }

    private List<LPCVariable> parametersToParameterList(LPCParser.Expr_listContext exprListContext) {
        List<LPCVariable> l = new LinkedList<>();
        extractParameterTo(l, exprListContext.expr_list2());
        return l;
    }

    private void extractParameterTo(List<LPCVariable> l, LPCParser.Expr_list2Context exprListContext) {
        if(exprListContext == null) return;

        l.add(visit(exprListContext.expr_list_node()));
        if(exprListContext.expr_list2() != null) {
            extractParameterTo(l, exprListContext.expr_list2());
        }
    }

    @Override
    public LPCVariable visitNew_local_def_declare_and_assign_single(LPCParser.New_local_def_declare_and_assign_singleContext ctx) {
        String varName = ctx.new_local_name().getText();
        LPCVariable value = visit(ctx.expr0());
        if(executionContext.getVariable(varName) != null) {
            throw new LPCRunTimeException("variable already defined: " + varName, new TokenErrorLocation(ctx.getStart()));
        }

        safeCall( () -> executionContext.setVariable(varName, value), ctx);
        return value;
    }

    @Override
    public LPCVariable visitLocal_declare_statement(LPCParser.Local_declare_statementContext ctx) {
        if(ctx.getText().contains("=")) {
            LPCVariable assignedVar = visit(ctx.local_name_list());
            LPCVariableType expectedType = LPCVariableType.fromString(ctx.basic_type().getText());
            if(expectedType == LPCVariableType.MIXED) {
                assignedVar.setMixed(true);
            }

            if(!assignedVar.isMixed() && assignedVar.type() != expectedType) {
                throw new LPCRunTimeException("type mismatch in assignment (" + assignedVar.type().name() + " vs " + expectedType.name() +")", new TokenErrorLocation(ctx.getStart()));
            }
            return assignedVar;
        }

        LPCVariableType dType = LPCVariableType.fromString(ctx.basic_type().getText());
        localDeclare(dType, ctx.local_name_list());
        return null;
    }

    private void localDeclare(LPCVariableType dType, LPCParser.Local_name_listContext localNameListContext) {
        String vName;
        LPCVariable variable = new LPCVariable(dType);
        vName = localNameListContext.new_local_def().getText();
        safeCall( () -> executionContext.setVariable(vName, variable), localNameListContext);
        if(localNameListContext.local_name_list() != null) {
            localDeclare(dType, localNameListContext.local_name_list());
        }
    }

    @Override
    public LPCVariable visitAssignmentPlusExpression(LPCParser.AssignmentPlusExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable value = visit(ctx.expr0());
        safeCall( () -> lpcVariable.setValue(returningSafeCall(() -> lpcVariable.add(value), ctx)), ctx);
        return lpcVariable;
    }

    private LPCVariable getVariableAssignToTarget(String ctx, ParserRuleContext parserContext) {
        LPCVariable lpcVariable = executionContext.getVariable(ctx);
        if (lpcVariable == null) {
            throw new LPCRunTimeException("undefined variable: " + ctx, new TokenErrorLocation(parserContext.getStart()));
        }
        return lpcVariable;
    }

    @Override
    public LPCVariable visitAssignmentSubtractExpression(LPCParser.AssignmentSubtractExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable value = visit(ctx.expr0());
        safeCall( () -> lpcVariable.setValue(lpcVariable.subtract(value)), ctx);
        return lpcVariable;
    }

    @Override
    public LPCVariable visitAssignmentMultiplyExpression(LPCParser.AssignmentMultiplyExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable value = visit(ctx.expr0());
        safeCall( () -> lpcVariable.setValue(lpcVariable.multiply(value)), ctx);
        return lpcVariable;
    }

    @Override
    public LPCVariable visitAssignmentDivideExpression(LPCParser.AssignmentDivideExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable value = visit(ctx.expr0());
        safeCall( () -> lpcVariable.setValue(lpcVariable.divideBy(value)), ctx);
        return lpcVariable;
    }

    @Override
    public LPCVariable visitAssignmentModuloExpression(LPCParser.AssignmentModuloExpressionContext ctx) {
        LPCVariable lpcVariable = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable value = visit(ctx.expr0());
        safeCall( () -> lpcVariable.setValue(lpcVariable.modulo(value)), ctx);
        return lpcVariable;
    }

    @Override
    public LPCVariable visitAssignmentBitAndExpression(LPCParser.AssignmentBitAndExpressionContext ctx) {
        LPCVariable left = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable right = visit(ctx.expr0());
        safeCall( () -> left.setValue(left.and(right)), ctx);
        return left;
    }

    @Override
    public LPCVariable visitAssignmentBitOrExpression(LPCParser.AssignmentBitOrExpressionContext ctx) {
        LPCVariable left = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable right = visit(ctx.expr0());
        safeCall( () -> left.setValue(left.or(right)), ctx);
        return left;
    }

    @Override
    public LPCVariable visitAssignmentBitXorExpression(LPCParser.AssignmentBitXorExpressionContext ctx) {
        LPCVariable left = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable right = visit(ctx.expr0());
        safeCall( () -> left.setValue(left.xor(right)), ctx);
        return left;
    }

    @Override
    public LPCVariable visitAssignmentLeftShiftExpression(LPCParser.AssignmentLeftShiftExpressionContext ctx) {
        LPCVariable left = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable right = visit(ctx.expr0());
        safeCall( () -> left.setValue(left.shiftLeft(right.intValue())), ctx);
        return left;
    }

    @Override
    public LPCVariable visitAssignmentRightShiftExpression(LPCParser.AssignmentRightShiftExpressionContext ctx) {
        LPCVariable left = getVariableAssignToTarget(ctx.expr4().getText(), ctx);
        LPCVariable right = visit(ctx.expr0());
        safeCall( () -> left.setValue(left.shiftRight(right.intValue())) , ctx);
        return left;
    }

    @Override
    public LPCVariable visitBinaryAndExpression(LPCParser.BinaryAndExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return left.and(right);
    }

    @Override
    public LPCVariable visitBinaryOrExpression(LPCParser.BinaryOrExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return left.or(right);
    }

    @Override
    public LPCVariable visitBinaryXorExpression(LPCParser.BinaryXorExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return left.xor(right);
    }

    @Override
    public LPCVariable visitLeftShiftExpression(LPCParser.LeftShiftExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return left.shiftLeft(right.intValue());
    }

    @Override
    public LPCVariable visitRightShiftExpression(LPCParser.RightShiftExpressionContext ctx) {
        LPCVariable left = visit(ctx.expr0(0));
        LPCVariable right = visit(ctx.expr0(1));
        return left.shiftRight(right.intValue());
    }

    @Override
    public LPCVariable visitBitwiseNotExpression(LPCParser.BitwiseNotExpressionContext ctx) {
        LPCVariable value = visit(ctx.expr0());
        return value.not();
    }

    @Override
    public LPCVariable visitExpr4CommaArray(LPCParser.Expr4CommaArrayContext ctx) {
        LPCVariable accessIndex = visit(ctx.comma_expr());
        LPCVariable target = visit(ctx.expr4());
        if(target.type() != LPCVariableType.STRING) {
            throw new LPCNotImplementedException("only string access is implemented", new TokenErrorLocation(ctx.getStart()));
        }
        return returningSafeCall( () -> target.at(accessIndex.intValue()), ctx);
    }

    @Override
    public LPCVariable visitMinusMinusExpression(LPCParser.MinusMinusExpressionContext ctx) {
        LPCVariable target = visit(ctx.expr4());
        safeCall( () -> target.setValue(target.subtract(LPCVariable.asInt("1"))), ctx);
        return target;
    }

    @Override
    public LPCVariable visitMinusMinusExpression4(LPCParser.MinusMinusExpression4Context ctx) {
        LPCVariable target = visit(ctx.expr4());
        LPCVariable oldValue = LPCVariable.copyFrom(target);
        safeCall( () ->target.setValue(target.subtract(LPCVariable.asInt("1"))), ctx);
        return oldValue;
    }

    @Override
    public LPCVariable visitPlusPlusExpression(LPCParser.PlusPlusExpressionContext ctx) {
        LPCVariable target = visit(ctx.expr4());
        safeCall( () -> target.setValue(target.add(LPCVariable.asInt("1"))), ctx);
        return target;
    }

    @Override
    public LPCVariable visitPlusPlusExpression4(LPCParser.PlusPlusExpression4Context ctx) {
        LPCVariable target = visit(ctx.expr4());
        LPCVariable oldValue = LPCVariable.copyFrom(target);
        safeCall( () -> target.setValue(target.add(LPCVariable.asInt("1"))), ctx);
        return oldValue;
    }

    @Override
    public LPCVariable visitReturnStatement(LPCParser.ReturnStatementContext ctx) {
        if(ctx.comma_expr() != null) {
            returnValue = visit(ctx.comma_expr());

            if(!Objects.equals(returnValue.type(), expectedReturnType)) {
                throw new LPCMismatchedReturnTypeException(expectedReturnType, returnValue.type(), new TokenErrorLocation(ctx.getStart()));
            }
        }

        continueParsing = false;

        return null;
    }

    @Override
    protected boolean shouldVisitNextChild(RuleNode node, LPCVariable currentResult) {
        return continueParsing;
    }

    public LPCVariable getReturnValue() {
        return returnValue;
    }
}
