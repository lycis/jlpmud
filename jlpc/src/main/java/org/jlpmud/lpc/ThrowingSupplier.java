package org.jlpmud.lpc;

@FunctionalInterface
public interface ThrowingSupplier<T> {
    T get() throws Exception;
}