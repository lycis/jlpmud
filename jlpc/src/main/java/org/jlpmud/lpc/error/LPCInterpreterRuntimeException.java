package org.jlpmud.lpc.error;

/**
 * Represents a runtime exception that occurs during interpreting LPC code that is not related to the code itself, but
 * the surrounding infrastructure. It indicates that the interpreter ran into a problem.
 */
public class LPCInterpreterRuntimeException extends RuntimeException {
    public LPCInterpreterRuntimeException(String message, Exception e) {
        super(message, e);
    }
}
