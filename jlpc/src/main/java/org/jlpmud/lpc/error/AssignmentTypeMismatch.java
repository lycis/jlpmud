package org.jlpmud.lpc.error;

import org.jlpmud.lpc.LPCVariableType;

public class AssignmentTypeMismatch extends Exception {
    public AssignmentTypeMismatch(LPCVariableType expectedType, LPCVariableType receivedType) {
      super("type mismatch in assignment (" + expectedType.name() + " vs " + receivedType.name() + ")");
    }
}
