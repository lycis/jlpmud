package org.jlpmud.lpc.error;

import org.jlpmud.lpc.LPCVariableType;

public class InvalidDataTypeExpression extends Exception {
    public InvalidDataTypeExpression(LPCVariableType expectedType, String givenExpression, Exception cause) {
        super("invalid "+expectedType.name()+" expression ("+givenExpression+")", cause);
    }
}
