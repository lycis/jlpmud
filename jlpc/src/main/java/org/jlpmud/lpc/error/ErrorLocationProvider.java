package org.jlpmud.lpc.error;

public interface ErrorLocationProvider {
    // tells us which line the error occurred on
    int getLine();

    // tells us the position of the error in the line
    int getPosition();
}
