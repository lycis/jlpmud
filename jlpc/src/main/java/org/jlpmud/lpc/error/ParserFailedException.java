package org.jlpmud.lpc.error;

public class ParserFailedException extends RuntimeException {
    public ParserFailedException(String message) {
        super(message);
    }
}
