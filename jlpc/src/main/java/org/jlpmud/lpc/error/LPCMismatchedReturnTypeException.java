package org.jlpmud.lpc.error;

import org.jlpmud.lpc.LPCVariableType;

public class LPCMismatchedReturnTypeException extends LPCRunTimeException {
    public LPCMismatchedReturnTypeException(LPCVariableType expectedReturnType, LPCVariableType type, ErrorLocationProvider loc) {
        super("mismatched return type: "+type+" returned for "+expectedReturnType, loc);
    }
}
