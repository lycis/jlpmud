package org.jlpmud.lpc.error;

import org.jlpmud.lpc.LPCVariableType;

public class InvalidCast extends Exception {
    public InvalidCast(LPCVariableType expectedType) {
        this(expectedType, null);
    }

    public InvalidCast(LPCVariableType expectedType, Exception cause) {
        super("invalid cast to "+expectedType.name(), cause);
    }
}
