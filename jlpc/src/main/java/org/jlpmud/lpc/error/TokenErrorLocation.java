package org.jlpmud.lpc.error;

import org.antlr.v4.runtime.Token;

public class TokenErrorLocation implements ErrorLocationProvider {

    private final Token token;

    public TokenErrorLocation(Token token) {
        this.token = token;
    }

    @Override
    public int getLine() {
        return token.getLine();
    }

    @Override
    public int getPosition() {
        return token.getCharPositionInLine();
    }
}
