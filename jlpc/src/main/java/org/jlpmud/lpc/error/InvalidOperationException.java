package org.jlpmud.lpc.error;

public class InvalidOperationException extends Exception {
    public InvalidOperationException(String message) {
        super("unsupported operation: " + message);
    }
}
