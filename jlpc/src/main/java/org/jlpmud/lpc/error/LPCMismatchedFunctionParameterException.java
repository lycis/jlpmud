package org.jlpmud.lpc.error;

import org.jlpmud.lpc.LPCFunctionArgumentDefinition;
import org.jlpmud.lpc.LPCVariableType;

public class LPCMismatchedFunctionParameterException extends LPCRunTimeException {
    public LPCMismatchedFunctionParameterException(LPCFunctionArgumentDefinition expectedArgument, LPCVariableType passedType, ErrorLocationProvider context) {
        super("mismatched function parameter '"+expectedArgument.name()+"': "+passedType+" passed for "+expectedArgument.type()+" expected", context);
    }
}
