package org.jlpmud.lpc.error;

public class LPCNotImplementedException extends LPCRunTimeException {
    public LPCNotImplementedException(String reason, ErrorLocationProvider context) {
        super("Not implemented: " + reason, context);
    }
}
