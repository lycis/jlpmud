package org.jlpmud.lpc.error;

/**
 * This exception type marks an error occurring during the execution of LPC code, usually caused by the code itself.
 */
public class LPCRunTimeException extends RuntimeException {
    private int lineNumber = -1;
    private int linePosition = -1;

    public LPCRunTimeException(String message, ErrorLocationProvider location) {
        super(message);

        setErrorLocation(location);
    }

    private void setErrorLocation(ErrorLocationProvider location) {
        if(location != null) {
            lineNumber = location.getLine();
            linePosition = location.getPosition();
        }
    }

    public LPCRunTimeException(String message, Throwable cause, ErrorLocationProvider location) {
        super(message, cause);

        setErrorLocation(location);
    }

    public int getLine() {
        return lineNumber;
    }

    public int getPosition() {
        return linePosition;
    }
}
