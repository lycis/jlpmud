package org.jlpmud.lpc;

import org.jlpmud.lpc.error.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

public class LPCVariable {
    private String valueExpr;
    private LPCVariableType dType;
    private boolean mixed = false;

    public LPCVariable(LPCVariableType dType) {
        this.dType = dType;
    }

    public LPCVariable(LPCVariableType dType, String valueExpr) {
        this(dType);
        this.valueExpr = valueExpr;
    }

    public LPCVariable(LPCVariableType lpcVariableType, int numericValue) {
        this(lpcVariableType, Integer.toString(numericValue));
    }

    public static LPCVariable asInt(String value) throws InvalidDataTypeExpression {
        LPCVariable variable     = new LPCVariable(LPCVariableType.INTEGER);
        try {
            variable.valueExpr = (new BigInteger(value)).toString();
        } catch(NumberFormatException e) {
            throw new InvalidDataTypeExpression(LPCVariableType.INTEGER, value, e);
        }

        return variable;
    }

    public static LPCVariable asFloat(String s) {
        LPCVariable variable = new LPCVariable(LPCVariableType.FLOAT);
        try {
            variable.valueExpr = Double.toString(Double.parseDouble(s));
        } catch(NumberFormatException e) {
            throw new LPCRunTimeException("invalid float expression ("+s+")", e, null);
        }

        return variable;
    }

    public LPCVariableType type() {
        return dType;
    }

    public BigInteger intValue() {
        if(valueExpr == null) return BigInteger.ZERO;

        try {
            return new BigInteger(valueExpr);
        } catch(NumberFormatException e) {
            throw new LPCRunTimeException("invalid integer expression ("+valueExpr+")", null);
        }
    }

    public void setValue(LPCVariable value) throws AssignmentTypeMismatch {
        if(mixed) {
            // 'mixed' variables mutate their type to whatever is assigned to them
            this.dType = value.dType;
        }

        if (!(this.type() == LPCVariableType.STATUS && value.type() == LPCVariableType.INTEGER) && this.type() != value.type()) {
                throw new AssignmentTypeMismatch(this.type(), value.type());
        }

        if(this.type() == LPCVariableType.STATUS && value.intValue().compareTo(BigInteger.ZERO) != 0 && value.intValue().compareTo(BigInteger.ONE) != 0) {
            throw new LPCRunTimeException("invalid status expression (" + value.intValue()+")", null);
        }

        this.valueExpr = value.valueExpr;
    }

    public String stringValue() {
        return this.valueExpr;
    }

    public LPCVariable add(LPCVariable value) throws InvalidOperationException, InvalidCast, InvalidDataTypeExpression {
        if(this.type() == LPCVariableType.STRING || value.type() == LPCVariableType.STRING) {
            return concatenateString(value);
        } else if(this.type() == LPCVariableType.INTEGER) {
            return addSomethingToInt(value);
        } else if(this.type() == LPCVariableType.FLOAT) {
            return addSomethingToFloat(value);
        }

        throw new InvalidOperationException("addition for type " + value.type() + " and " + this.type());
    }

    private LPCVariable concatenateString(LPCVariable value) throws InvalidCast {
        return new LPCVariable(LPCVariableType.STRING, this.stringValue() + value.cast(LPCVariableType.STRING).stringValue());
    }

    private LPCVariable addSomethingToFloat(LPCVariable value) throws InvalidOperationException, InvalidDataTypeExpression, InvalidCast {
        if(value.type() == LPCVariableType.INTEGER || value.type() == LPCVariableType.FLOAT) {
            return new LPCVariable(LPCVariableType.FLOAT, this.floatValue() + value.castToFloat().floatValue() + "");
        }
        throw new InvalidOperationException("addition for type " + value.type() + " and " + this.type());
    }

    private LPCVariable addSomethingToInt(LPCVariable value) throws InvalidOperationException, InvalidCast {
        if(value.type() == LPCVariableType.FLOAT) {
            return new LPCVariable(LPCVariableType.INTEGER, this.intValue().add(value.cast(LPCVariableType.INTEGER).intValue()).toString());
        } else if(value.type() == LPCVariableType.INTEGER) {
            return new LPCVariable(LPCVariableType.INTEGER, this.intValue().add(value.intValue()).toString());
        }

        throw new InvalidOperationException("addition for type " + value.type() + " and " + this.type());
    }

    public LPCVariable cast(LPCVariableType lpcVariableType) throws InvalidCast {
        if (Objects.requireNonNull(lpcVariableType) == LPCVariableType.INTEGER) {
            return castToInteger();
        } else if(lpcVariableType == LPCVariableType.FLOAT) {
            return castToFloat();
        } else if(lpcVariableType == LPCVariableType.STRING) {
            return new LPCVariable(LPCVariableType.STRING, this.stringValue());
        }
        throw new InvalidCast(LPCVariableType.STRING);
    }

    private LPCVariable castToFloat() throws InvalidCast {
        return switch (this.type()) {
            case INTEGER -> new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.intValue().doubleValue()));
            case FLOAT -> new LPCVariable(LPCVariableType.FLOAT, this.valueExpr);
            default -> throw new InvalidCast(LPCVariableType.FLOAT);
        };
    }

    private LPCVariable castToInteger() throws InvalidCast {
        switch (this.type()) {
            case INTEGER:
                return new LPCVariable(LPCVariableType.INTEGER, this.valueExpr);
            case FLOAT:
                try {
                    return new LPCVariable(LPCVariableType.INTEGER, BigDecimal.valueOf(this.floatValue()).toBigInteger().toString());
                } catch (InvalidDataTypeExpression e) {
                    throw new InvalidCast(LPCVariableType.FLOAT, e);
                }
            default:
                throw new InvalidCast(LPCVariableType.INTEGER);
        }
    }

    public LPCVariable subtract(LPCVariable value) throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        if(Objects.requireNonNull(this.type()) == LPCVariableType.INTEGER) {
            if(value.type() == LPCVariableType.FLOAT) {
                return new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.castToFloat().floatValue() - value.floatValue()));
            }
            return new LPCVariable(LPCVariableType.INTEGER, this.intValue().subtract(value.intValue()).toString());
        } else if(this.type() == LPCVariableType.FLOAT) {
            if(value.type() == LPCVariableType.INTEGER) {
                return new LPCVariable(LPCVariableType.FLOAT, this.floatValue() - value.castToFloat().floatValue() + "");
            }

            return new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.floatValue() - value.floatValue()));
        }

        throw new InvalidOperationException("subtraction for type " + this.type().name());
    }

    public LPCVariable multiply(LPCVariable right) throws InvalidCast, InvalidOperationException, InvalidDataTypeExpression {
        if(Objects.requireNonNull(this.type()) == LPCVariableType.INTEGER) {
            if(right.type() == LPCVariableType.FLOAT)
                return new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.castToFloat().floatValue() * right.floatValue()));
            else
                return new LPCVariable(LPCVariableType.INTEGER, this.intValue().multiply(right.intValue()).toString());
        } else if (this.type() == LPCVariableType.FLOAT) {
            return new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.floatValue() * right.floatValue()));
        }

        throw new InvalidOperationException("multiplication for type " + this.type().name());
    }

    public LPCVariable divideBy(LPCVariable right) throws InvalidOperationException, InvalidDataTypeExpression {
        if(Objects.requireNonNull(this.type()) == LPCVariableType.INTEGER) {
            return new LPCVariable(LPCVariableType.INTEGER, this.intValue().divide(right.intValue()).toString());
        } else if(this.type() == LPCVariableType.FLOAT) {
            return new LPCVariable(LPCVariableType.FLOAT, Double.toString(this.floatValue() / right.floatValue()));
        }

        throw new InvalidOperationException("division for type " + this.type().name());
    }

    public LPCVariable modulo(LPCVariable right) throws InvalidOperationException {
        if(Objects.requireNonNull(this.type()) == LPCVariableType.INTEGER) {
            return new LPCVariable(LPCVariableType.INTEGER, this.intValue().mod(right.intValue()).toString());
        }

        throw new InvalidOperationException("modulo for type " + this.type().name());
    }

    public LPCVariable and(LPCVariable right) {
        return new LPCVariable(LPCVariableType.INTEGER, this.intValue().and(right.intValue()).toString());
    }

    public LPCVariable or(LPCVariable right) {
        return new LPCVariable(LPCVariableType.INTEGER, this.intValue().or(right.intValue()).toString());
    }

    public LPCVariable xor(LPCVariable right) {
        return new LPCVariable(LPCVariableType.INTEGER, this.intValue().xor(right.intValue()).toString());
    }

    public LPCVariable not() {
        return new LPCVariable(LPCVariableType.INTEGER, this.intValue().not().toString());
    }

    public LPCVariable shiftLeft(BigInteger n) {
        return new LPCVariable(LPCVariableType.INTEGER, this.intValue().shiftLeft(n.intValue()).toString());
    }

    public LPCVariable shiftRight(BigInteger i) {
       return new LPCVariable(LPCVariableType.INTEGER, this.intValue().shiftRight(i.intValue()).toString());
    }

    public double floatValue() throws InvalidDataTypeExpression {
        if(valueExpr == null) return 0.0;

        try {
            return Double.parseDouble(valueExpr);
        } catch(NumberFormatException e) {
            throw new InvalidDataTypeExpression(LPCVariableType.FLOAT, valueExpr, e);
        }
    }

    public LPCVariable at(BigInteger index) throws InvalidOperationException {
        try {
            return new LPCVariable(LPCVariableType.INTEGER, this.stringValue().charAt(index.intValue()));
        } catch(StringIndexOutOfBoundsException e) {
            throw new InvalidOperationException("Illegal index " + index.intValue() + " for length " + stringValue().length());
        }
    }

    public static LPCVariable copyFrom(LPCVariable variable) {
        return new LPCVariable(variable.type(), variable.stringValue());
    }

    public boolean isMixed() {
        return mixed;
    }

    public void setMixed(boolean mixed) {
        this.mixed = mixed;
    }

    public String toString() {
        return "LPCVariable{" +
                "valueExpr='" + valueExpr + '\'' +
                ", dType=" + dType +
                ", mixed=" + mixed +
                '}';
    }
}
