# Including jlpc as depdency
Currently none of the projects archives or files are replicated to any of the publically available default maven repositories (e.g. maven central). If you wish to include them you can use the gitlab repository of the project in these ways:

## Build Locally
The currently best supported way is to simply check out the project and build it yourself:

```bash
# Clone the repository
git clone https://gitlab.com/lycis/jlpmud.git

# Build the project
cd jlpmud
mvn clean install
```

This way the `jlpc.jar` will be built into your local repository and you can then use it with the maven coordinates provided below.

## Direct Download
You can simply navigate to [the project's gitlab presence](https://gitlab.com/lycis/jlpmud) and download the latest build of the `jlpc.jar`.

## Use the Project repository
In order to include the dependencies into your own `maven` or `gradle` projects you first need to define the project artifact repository in your `pom.xml` or `settings.xml`:

```xml
<!-- pom.xml -->
<repositories>
  <repository>
    <id>gitlab-jlpmud-maven</id>
    <url>https://gitlab.com/api/v4/projects/lycis%2Fjlpmud/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-jlpmud-maven</id>
    <url>https://gitlab.com/api/v4/projects/lycis%2Fjlpmud/packages/maven</url>
  </repository>
  <snapshotRepository> <id>gitlab-jlpmud-maven</id>
    <url>https://gitlab.com/api/v4/projects/lycis%2Fjlpmud/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

After that you can use the following maven cooridinates:

```xml
<depdency>
    <groupId>org.jlmpud</groupId>
    <artifactId>jlpc</artifactId>
    <version>1.0-SNAPSHOT</version>
</depdency>
```
