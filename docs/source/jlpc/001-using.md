# Using JLPC to parse LPC Code

## Overview

The `LPCInterpreter` is the entry point for interpreting LPC code in the `jlpc` library. It processes LPC code and returns an `LPCObject`, which holds the defined functions and global variables within the code.

## Getting Started

### Interpreting LPC Code

To interpret LPC code, use the `LPCInterpreter` class and its `interpretProgramAsObject(...)` method, which returns an `LPCObject` representing the program's execution context. This object allows you to access functions and variables declared in the LPC program.

### Example Usage

```java
import org.jlpmud.lpc.LPCInterpreter;
import org.jlpmud.lpc.LPCObject;

public class LPCInterpreterExample {
    public static void main(String[] args) {
        String lpcCode = "int my_var = 42; void my_function() { write(\"Hello, LPC!\"); }";
        
        // Interpret the LPC code
        var interpreter = new LPCInterPreter();
        LPCObject programObject = interpreter.interpretProgramAsObject(lpcCode);
        
        // Access a global variable
        System.out.println("my_var = " + programObject.getVariable("my_var"));
        
        // Access a function
        if (programObject.hasFunction("my_function")) {
            System.out.println("Function my_function is defined in the program.");
        }
    }
}
```

### Methods

#### `interpretProgramAsObject(String code)`
- **Description**: Interprets the provided LPC code and returns an `LPCObject`.
- **Parameters**:
  - `code`: The LPC code to be interpreted as a `String`.
- **Returns**: An `LPCObject` representing the code's functions and variables.

### LPCObject

The `LPCObject` class holds the global execution context of the interpreted LPC program. It stores declared functions and variables.

#### Methods

- **`getFunctions()`**: Returns a `Map<String, LPCFunction>` containing the functions declared in the program.
- **`getGlobalVariables()`**: Returns a `Map<String, LPCVariable>` containing the global variables.
- **`getVariable(String variableName)`**: Returns the value of a global variable by name.
- **`hasFunction(String functionName)`**: Returns `true` if the specified function exists in the program.
- **`getFunction(String functionName)`**: Retrieves a function by name.

#### Example

```java
// Access a global variable
LPCVariable myVar = programObject.getVariable("my_var");

// Set a new global variable
programObject.setVariable("new_var", new LPCVariable(100));

// Retrieve a function and invoke it (assuming LPCFunction has an invoke method)
LPCFunction myFunction = programObject.getFunction("my_function");
if (myFunction != null) {
    myFunction.invoke();
}
```

### Error Handling
There are two kinds of errors:
1. Errors that occur _during the interpretation_ of LPC code are thrown as `LPCInterpreterRuntimeException`. 
2. Errors generated _while parsing the code_ are thrown by the `LPCInterpreter` as `ParserFailedException`. 

You can catch and handle these exceptions to manage error scenarios when loading and interpreting the LPC code.

#### Example:

```java
try {
    LPCObject programObject = LPCInterpreter.interpretProgramAsObject(lpcCode);
} catch (LPCInterpreterRuntimeException e) {
    System.err.println("Error during interpretation at line "+e.getLine()+":"+e.getPosition()+": " + e.getMessage());
} catch(ParserFailedException e) {
    System.err.println("Failed to load code: "+e.getMessage());
}
```

This allows you to safely manage errors, ensuring that your application doesn't crash due to parsing issues in LPC code.
