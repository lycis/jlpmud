# JLPC Interpreter

The package `org.jlpmud.jlpc` contains all classes that together make up the LPC language interpreter of `jlpmud`. This module of the project is distributed as a `jar` file and can be used to build your own tools or projects on top of it.

:::{admonition} LPC capabilities are evolving
:class: warning

Until the project reaches a stable version (e.g. `1.0.0`) the LPC language capabilities are not stable and will be growing. If you build upon these resources before that, be aware of changing interfaces and function without previous announcement.
::: 

The following pages describe how to install and use the `jlpc` package as language interpreter for LPC. There is also a part on some noteworthy technical details of the implementation, in case you are very curious how it works or wish to contribute to the project.

## Contents
```{toctree} 
:maxdepth: 2

000-including
001-using
```

