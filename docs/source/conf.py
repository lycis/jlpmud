# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'JLPMud'
copyright = '2024, Daniel Eder'
author = 'Daniel Eder'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['myst_parser']
source_suffix = {
        '.rst': 'restructuredtext',
        '.txt': 'markdown',
        '.md': 'markdown'
}

templates_path = ['_templates']
exclude_patterns = []

myst_enable_extensions = [
    "amsmath",
    "attrs_inline",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    # "linkify",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist",
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
html_theme_options = {
        "use_repository_button": True,
        "use_issues_button": True,
        "repository_provider": "gitlab",
        "repository_url": "https://gitlab.com/lycis/jlpmud/",
        "icon_links": [
            {
                "name": "Build Status",
                "url": "https://gitlab.com/lycis/jlpmud/-/pipelines",
                "icon": "https://img.shields.io/gitlab/pipeline-status/lycis%2Fjlpmud?style=for-the-badge",
                "type": "url"
            },
            {
                "name": "Sonar Quality Gate",
                "url": "https://img.shields.io/sonar/quality_gate/lycis_jlpmud?server=https%3A%2F%2Fsonarcloud.io&style=for-the-badge",
                "icon": "https://img.shields.io/sonar/quality_gate/lycis_jlpmud?server=https%3A%2F%2Fsonarcloud.io&style=for-the-badge",
                "type": "url"
            }
        ]
}
