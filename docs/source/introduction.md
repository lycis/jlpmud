# Getting Sarted Guide
Read here if you want to get started with setting up the `jlpcmud` tool and ecosystem with its current state.

## Installation
:::{admonition} Using as dependency
:class: warning

Currently no part of `jlpmud` is released in any package repository (e.g. maven central). This will only happen once it reaches 1.0.0 maturity.

Before that, you can either build it yourself, download the packages or use the gitlab project repository as your maven repo.
:::

## Load & Interact with Code
tbd
