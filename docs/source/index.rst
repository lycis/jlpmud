jlpmud's Documentation
===================================

.. code-block:: ascii

       ██╗██╗     ██████╗ ███╗   ███╗██╗   ██╗██████╗ 
       ██║██║     ██╔══██╗████╗ ████║██║   ██║██╔══██╗
       ██║██║     ██████╔╝██╔████╔██║██║   ██║██║  ██║
  ██   ██║██║     ██╔═══╝ ██║╚██╔╝██║██║   ██║██║  ██║
  ╚█████╔╝███████╗██║     ██║ ╚═╝ ██║╚██████╔╝██████╔╝
   ╚════╝ ╚══════╝╚═╝     ╚═╝     ╚═╝ ╚═════╝ ╚═════╝                      
  a lpmud3.0 implementation in java
    ... fully in java
    ... supporting tool ecosystem
    ... full LPC compatibility

  > java -jar jlpmud
  JLPMud 0.0.1 starting...


**jlpmud** is an experimental project aimed at re-implementing the LPMud driver in Java. This documentation provides a comprehensive guide to using and contributing to the project, as well as details on the LPC parser (`jlpc`), command-line tool (`jlpcc`), and the upcoming driver.

The project is hosted on GitLab and can be found here:  
`jlpmud GitLab Repository <https://gitlab.com/lycis/jlpmud/>`_

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents

   introduction
   /lpc/index
   /jlpc/index
   /tools/index
   roadmap

Overview
--------

**jlpmud** consists of three main components:

- **jlpc (LPC Parser)**: A Java library designed to parse and interpret LPC code.
- **jlpcc (Command-Line Tool)**: A tool for loading, interpreting, and interacting with LPC code via the command line.
- **Driver (Upcoming)**: A Java-based re-implementation of the LPMud driver, which is still in development.

Documentation Sections:
------------------------

1. **Introduction to jlpmud**
   - Project overview, features, installation, and contributing guidelines.
   
2. **LPC Language Documentation**
   - Description of the LPC language features supported by the project, including syntax and known limitations.
   
3. **jlpc Library Documentation**
   - Detailed information on the `jlpc` Java library, its architecture, and how to use it for parsing LPC code.
   
4. **jlpcc Tool Guide**
   - Instructions for building and using the `jlpcc` tool, along with examples of loading and interpreting LPC code.
   
5. **Driver Roadmap**
   - A section covering the progress and planned features for the upcoming driver implementation.
   
6. **Additional Resources**
   - External references, further reading, and related projects.

Getting Started
---------------

To get started with jlpmud, refer to the following documentation sections:

- Installation guide: See :doc:`introduction`
- Using the jlpc library to interpret LPC code: See :doc:`jlpc/index`
- Supported LPC language features: See :doc:`lpc/index`
- The command line utility `jlpcc` to interact with code: See :doc:`tools/000-jlpcc`

Help and Contributions
-----------------------

For further assistance or to contribute to the project, visit the `GitLab project <https://gitlab.com/lycis/jlpmud/>`_ and check the `issues section <https://gitlab.com/lycis/jlpmud/-/issues>`_.

.. note::
   This project is experimental and not intended for production use. Feedback and contributions are welcome!

