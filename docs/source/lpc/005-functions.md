# Functions
:::{admonition} Limited implementation
:class: warning
Currently functions are only supported for basic data types (`int`, `float`, `status`, `string`, `status` and `mixed`).

At this time, other data types (e.g., `object`) are not supported. Support for these types may be added in future releases.
:::

```{contents}
```

## Function Declaration

Functions can be declared with any of the basic types as their return type and parameter types. The general syntax for declaring a function is:

```c
<return_type> <function_name>(<parameter_type> <parameter_name>, ...) {
    // Function body
}
```

Where:
- `<return_type>` is the type the function will return.
- `<function_name>` is the name of the function.
- `<parameter_type>` and `<parameter_name>` specify the types and names of the parameters.

### Example Function Definitions

Here are examples of functions using basic types:

```{code-block}
int add(int a, int b) {
    return a + b;
}

float divide(float a, float b) {
    if (b == 0.0) {
        return -1.0; // Handle division by zero
    }
    return a / b;
}

string concatenate(string str1, string str2) {
    return str1 + str2;
}

status is_positive(int number) {
    return number > 0;
}

mixed get_value(status condition) {
    if (condition) {
        return 42;  // Returns an integer
    } else {
        return "No value";  // Returns a string
    }
}
```

## Calling Functions

Functions can be called using the standard function invocation syntax:

```c
int result = add(10, 5);                // result = 15
float quotient = divide(10.0, 2.0);     // quotient = 5.0
string fullName = concatenate("John", "Doe");  // fullName = "JohnDoe"
status check = is_positive(3);          // check = 1
mixed value = get_value(1);             // value = 42
```

## Parameter and Return Types
Functions, in large parts, behave like variables and are strongly typed. Passing wrong parameter datatypes as well 
as returning the wrong data type from a `return`-Statement will cause according errors. 

```{code-block} c
:caption: Example - Mismatching parameter types
   
int add(int a, int b) {
    return a + b;
}

// This will result in an error due to a type mismatch for parameter a
int result = add("10", 5);
```

```{code-block} c
:caption: Example - Mistmatching return type

int add(int a, int b) {
    // causes an error due to a type mismatch with the return type
    return "The result is: " + (a+b);
}
```