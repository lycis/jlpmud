# LPC Basics

## Why Choose LPC?

LPC stands out for its ability to handle dynamic, interactive applications with ease. Whether you’re building multiplayer games, real-time simulations, or any system requiring rapid prototyping and live interaction, LPC provides the structure and flexibility needed to make development smooth and efficient.

In this documentation, you'll find everything you need to start coding in LPC using jlpc. As you progress, you'll explore how LPC's design principles—modularity, real-time interaction, and object orientation—can empower you to create complex and responsive systems with minimal overhead.

## LPC Programs
LPC programs are typically object-oriented, with a focus on defining objects (such as characters, rooms, items, etc.) and their behaviors (methods). Here's a simple example of a basic LPC program that defines a room in an interactive environment:

```c
inherit "/std/room";  // Inheriting from a standard room object

void setup() {
    set_short("A simple room");
    set_long("This room is empty except for a wooden chair.");
    add_exit("north", "/rooms/north_room");
    add_item("chair", "A plain wooden chair with a worn seat.");
}
```


