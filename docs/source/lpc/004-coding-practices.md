# Good LPC Coding Practices
This part details some good practices when creating LPC code. These are not enforced rules, but general advice collected over multiple decades of coding LPC in differend games.

```{contents}
```

## Common Pitfalls to Avoid
### Null References
Be cautious when dealing with variables that may be uninitialized or null. Always check that objects and variables are properly initialized before using them.

**Example:**
```c
if (!player) {
    write("Player not found.");
    return;
}
```

### Unchecked Type Mismatches
Apart from realing with variables that are `0` or uninitialized, you have to pay attention to the typesystem of LPC. While LPC may appear rather strongly typed, it is in reality not. This is very much explicit when dealing with the [mixed](001-datatypes#mixed) datatype, but can happen at any point.

```c
int add(int a, int b) {
    return a+b;
}

add("hello", "world");
```

Believe it or not, the code above works in LPC. It will result in an uncaught error though. To prevent this, implement typechecking when in doubt:

```c
int add(int a, int b) {
    if(!intp(a) || !intp(b)) return 0;
    return a+b;
}
```

Functions like `intp(...)`, `floatp(...)`, etc. allow to check for type-correctness of variables. Especially when using `mixed` this required.

### Infinite Loops
Pay attention to loops that may not terminate correctly, especially in real-time environments where they can cause the system to hang.

**Example:**
```c
int i;
i = 0;
while(i<10) {
  write(sprintf("i = %d\n", i));
}
```

Usually the driver will kill code that takes too long to execute in order to keep the game loop running. Still be ware of such conditions. They may not always be that obvious.

### Memory Leaks
LPC has automatic memory management, but it's still important to clean up references to objects no longer in use to prevent memory issues.

A very typical example how such a memory leak may occur has to do with the object lifecycle. Look at this code:

```c
void giveAStick() {
  object ob;
  ob = clone_object("/obj/stick");
  ob->move(this_player(), M_GET);
  return;
}
```

The problem is, that the newly cloned stick-object (`ob`) may get lost dangling in the object table of the driver if the move fails. After exiting the function the object is literally in the void of the driver.

To prevent this, always make sure that you explicitly destruct objects (and other allocated memories) in case of errors or when no longer needed.
