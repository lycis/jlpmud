# LPC Language Documentation
The JLPMud parser supports coding in LPC (Lars Pensjö C or LPMud C) as a first class citizen and native original language. These pages describe the full scope of the supported language features by the parser with code examples.

## Compatibility

:::{admonition} LPC Compatibility - lpmud 03.02.1@148p3
:class: note

The implementation of LPC is guaranteed to be compatible to the feature set of the `lpmud` driver in version `3.2.1@148p3`.
:::

## Content
This introduction provides a welcoming overview for developers using the LPC language, with `jlpc` as the interpreter, and sets the stage for the detailed technical documentation that follows. It emphasizes the language's key features, structure, and use cases, while also giving a simple example to help developers immediately connect with the language.


Explore the following sections for in-detail topics:

```{toctree} 
:maxdepth: 2

000-basic-lpc.md
001-datatypes.md
005-functions.md
002-oo-concepts.md
003-error-handling.md
004-coding-practices.md
```

We hope this documentation helps you get the most out of LPC and jlpc. Whether you're new to the language or looking to deepen your understanding, the sections that follow will guide you through every aspect of programming with LPC.
