# Datatypes & Variables

In `jlpc`, the following datatypes are supported. Each datatype supports various operations, such as assignment, arithmetic, comparisons, and more, depending on the type. This section outlines the operations supported for each datatype.

```{contents}
:depth: 3
```

## Datatype Overview

| Datatype |        Usage        |                        Operations                          |
| -------- | ------------------- | ---------------------------------------------------------- |
|   `int`  | Whole numbers        | `+`, `-`, `*`, `/`, `%`, `==`, `!=`, `<`, `>`, `<=`, `>=`, `&`, `|`, `^`, `~`, `<<`, `>>`, `=`, `+=`, `-=`, `*=`, `/=`, `%=`, `<<=`, `>>=` |
|  `float` | Real numbers (with decimals) | `+`, `-`, `*`, `/`, `==`, `!=`, `<`, `>`, `<=`, `>=`, `=`, `+=`, `-=`, `*=`, `/=` |
| `string` | Text manipulation    | `+` (concatenation), `==`, `!=`, `<`, `>`, `<=`, `>=`, `=` |
| `status` | Boolean values (true/false) | `&&`, `||`, `!`, `==`, `!=`, `=`                               |
| `mixed`  | Flexible (can hold any type) | Depends on runtime type, supports `+`, `-`, `*`, `/`, `==`, `!=`, `<`, `>`, `<=`, `>=`, `=` |
| `object` | Object reference (not yet supported) | Not applicable                                                 |

## Simple Datatypes
Simple datatypes, or *primitives*, are a very basic unit of storing a mutable variable in LPC. They hold a single, typed, value and allow for a basic set of operations.

### int
The `int` datatype represents whole numbers and supports arithmetic, comparison, and bitwise operations.

#### Supported Operations
- Arithmetic: `+`, `-`, `*`, `/`, `%` (modulo)
- Comparison: `==`, `!=`, `<`, `>`, `<=`, `>=`
- Bitwise: `&`, `|`, `^`, `~`, `<<`, `>>`
- Assignment: `=`, `+=`, `-=`, `*=`, `/=`, `%=`

#### Example
```c
int a = 10;
int b = 20;
int sum = a + b;
int difference = a - b;
int product = a * b;
int quotient = a / 2;
int remainder = a % 3;
int shifted = a << 1;
int bitwise_and = a & b;
write(sum);  // Outputs: 30
```

### float
The `float` datatype supports real numbers and provides arithmetic and comparison operations.

#### Supported Operations
- Arithmetic: `+`, `-`, `*`, `/`
- Comparison: `==`, `!=`, `<`, `>`, `<=`, `>=`
- Assignment: `=`, `+=`, `-=`, `*=`, `/=`

#### Example
```c
float x = 10.5;
float y = 2.3;
float result = x * y;
float division = x / y;
float sum = x + y;
write(result);  // Outputs: 24.15
```

### string
The `string` datatype allows for text manipulation and supports concatenation and comparison operations.

#### Supported Operations
- Concatenation: `+`
- Comparison: `==`, `!=`, `<`, `>`, `<=`, `>=`
- Assignment: `=`
- Access: `[n]`, `[a..b]`

#### Example
```c
string first_name = "John";
string last_name = "Doe";
string full_name = first_name + " " + last_name;
if (first_name == "John") {
    write("Hello, John!");
}
write(full_name);  // Outputs: John Doe
write(full_name[2]); // Outputs: h
write(full_name[3..5]); // Outputs: n D
write(full_name[5..]); // Outputs: Doe
```

### status
The `status` datatype is used for logical checks and supports logical operations.

#### Supported Operations
- Logical: `&&` (AND), `||` (OR), `!` (NOT)
- Comparison: `==`, `!=`
- Assignment: `=`

#### Example
```c
status is_active = 1;  // true
status has_permission = 0;  // false

if (is_active && has_permission) {
    write("System is active and user has permission.");
} else {
    write("Access denied.");
}
// Outputs: Access denied.
```

### mixed
The `mixed` datatype can hold any type and allows for flexible variable usage. Supported operations depend on the runtime type of the variable.

#### Supported Operations
*depending on contained datatype*

#### Example
```c
mixed variable;
variable = 42;  // Initially an int
write(variable);  // Outputs: 42

variable = "Now a string";
write(variable);  // Outputs: Now a string

variable = 3.14;  // Now a float
variable = variable * 2;
write(variable);  // Outputs: 6.28
```

## Complex Datatypes
Complex datatypes are larger structures instead of single primitive values. They are implemented through references to the actual data that is somewhere in the memory of the driver.

### object
```{warning}
The `object` datatype is currently not supported. It will be part of a later release.
```

### Arrays
```{warning}
Arrays are currently not supported. It will be part of a later release.
```

### mapping
```{warning}
The `mapping` datatype is currently not supported. It will be part of a later release.
```
