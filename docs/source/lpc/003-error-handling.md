# Error Handling and Debugging

Error handling and debugging are critical when developing complex systems in LPC. Properly managing errors helps prevent crashes and ensures smoother execution in real-time environments. Here are some best practices for error handling and debugging in jlpc.

:::{admonition} Not implemented
:class: warning

The concepts describes here are not yet supported in neither the interpreter nor the driver. See [](/roadmap.md) for details.
:::

:::{admonition} Improving the Toolchain
:class: hint

One reason why `jlpmud` was created in the first place was to provide better tooling around the creation and debugging of MUD code. THe upcoming releases will include an improved toolchain that relies less on in-game or in-code (e.g. printing) for debugging.
:::

```{contents}
```

## Best Practices for Error Handling in Code
### Use Try-Catch Blocks
LPC supports basic error handling with catch statements. Using catch blocks can help trap runtime errors without crashing the whole program.

**Example:**
```c
int divide(int x, int y) {
    if (y == 0) {
        catch {
            write("Error: Division by zero.");
            return 0;
        }
    }
    return x / y;
}
```

In this example, if a division by zero is attempted, the catch block handles the error gracefully, preventing the program from crashing.

### Check Input Validity
Always validate user input and other external data before using it in operations. This helps prevent unexpected errors due to invalid data.

**Example:**
```c

    int get_age(string input) {
        if (!input || sscanf(input, "%d", age) != 1 || age < 0) {
            write("Invalid age entered.");
            return -1;
        }
        return age;
    }
```

## Useful Design Principles
### Graceful Failures
Design your program to fail gracefully. If an error occurs, ensure the system can recover or provide meaningful feedback to the player or developer rather than failing silently or crashing.



###  Code Modularity
Keep your code modular. Small, independent functions are easier to debug than large, monolithic blocks of code. Test each function individually before integrating it into the larger system.

## Debugging Tools
LPC offers several debugging tools and strategies to help developers identify and fix issues:

### Log Files
Write detailed logs for significant actions, errors, or critical sections of code. This will help you trace what happened leading up to an error.

**Example:**
```c
log_file("error_log", "Error in function X at " + ctime(time()) + "\n");
```

### Use debug_info
The debug_info function provides information about the current state of an object, variable, or the overall system. This is useful for tracking down bugs related to object states or memory.

**Example:**
```c
write(debug_info(1, this_object()));  // Outputs information about the current object
```

