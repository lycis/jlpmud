# Object-Oriented Concepts

:::{admonition} Not implemented
:class: warning

The concepts describes here are not yet supported in neither the interpreter nor the driver. See [](/roadmap.md) for details.
:::

LPC is inherently an object-oriented language, where everything is built around the concept of objects. Each object can contain data (variables) and methods (functions) that define its behavior. The object-oriented approach in LPC allows for modular and reusable code.

```{contents}
```

## The Object Lifecycle
tbd

## Defining and Using Objects

Objects in LPC are typically defined by creating "blueprints" or classes that describe their properties and methods. You can create a new object by using the `new` keyword.

**Example:**
```c
inherit "/std/room";  // Inherit from a standard room object

void setup() {
    set_short("A cozy room");
    set_long("This is a small, cozy room with a warm fireplace.");
    add_item("fireplace", "A crackling fireplace providing warmth.");
    add_exit("north", "/rooms/north_room");
}
```

In this example:
- The `inherit` statement indicates that this object inherits properties and methods from the base object `/std/room`.
- The `setup` function defines the room’s description and interactions.

### Inheritance

Inheritance allows one object to acquire the properties and methods of another. This helps avoid code duplication and promotes reuse. In the above example, the room object inherits functionality from `/std/room`, meaning it already has built-in methods for handling exits, items, etc.

**Example:**
```c
inherit "/std/weapon";  // Inherit functionality from the weapon class

void setup() {
    set_name("sword");
    set_short("A sharp sword");
    set_long("A finely crafted sword with a gleaming blade.");
    set_damage(10);
}
```

### Encapsulation

Encapsulation is the practice of keeping data private within an object, exposing only necessary parts to the outside world. In LPC, variables and methods are generally encapsulated within objects, accessible only through public methods. This enhances modularity and protects object integrity.

**Example:**
```c
private int health;

void set_health(int h) {
    health = h;
}

int get_health() {
    return health;
}
```

Here, `health` is a private variable that can only be accessed or modified via the public `set_health` and `get_health` methods.

