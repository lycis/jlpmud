# jlpcc - CLI to interact with LPC code

## Overview

`jlpcc` is a command-line utility designed to load and interact with LPC (Lars Pensjö C) code through the `jlpc` interpreter. This tool allows users to execute LPC scripts, enter interactive debugging sessions, and manipulate variables in real time. It is particularly useful for developing and testing code in dynamic, interactive environments such as MUDs (Multi-User Dungeons).

## Getting Started

### Prerequisites

Before using `jlpcc`, ensure you have the following:
- Java Runtime Environment (JRE) installed on your machine.
- The `jlpcc` tool package downloaded and ready to execute.

Refer to the [Installation Guide](/introduction.md#installation) in the[](/introduction.md).

### Running jlpcc

To run `jlpcc`, use the following command structure:

```bash
java -jar jlpcc.jar [options] [files]
```

### Command-Line Options

- `-h`, `--help`: Displays help information about the available options and usage.
- `-i`, `--interactive`: Runs the code in interactive mode, allowing for real-time manipulation and debugging.

## Loading and Interpreting LPC Files

To interpret one or more LPC files, use the following command:

```bash
java -jar jlpcc.jar my_script.lpc
```

This command will load and execute `my_script.lpc`. If you have multiple files, list them all:

```bash
java -jar jlpcc.jar file1.lpc file2.lpc
```

## Running in Interactive Mode

To enter interactive mode, include the `-i` option:

```bash
java -jar jlpcc.jar -i my_script.lpc
```

In interactive mode, you can type LPC commands and debug directives and execute them on the fly. The prompt will appear as follows:

```
Entering interactive mode. Type '!exit' to leave.
LPC>
```

:::{admonition} Limitations of Interatice Mode
:class: important

Interactive mode has some limitations you need to be aware of for maximum usage. These are due to the way `jlpcc` reads, interprets and calls code in interactive mode.

When using `jlpcc` in interactive mode, you have to know that any code you input will be encapsulated within a injected LPC function and called on each new prompt you enter.

This leads to the following caveats:

1. **You cannot define functions in interactive mode.** As any code you enter into the prompt will be encapsulated within its own function. Defining a function within a function is not supported.
2. **You can only interact with global variables.** Although you ma call any function you like or rewrite any variable within the injected code, you can only statefully (e.g. through `!print`) interact with any variable that is globally defined within the object.
:::

### Executing LPC in Interactive Mode

In interactive mode, you can enter LPC code on the fly. The tool will package your code into an injected function and execute it whenever you enter new code.

**Example:**
```
$ java -jar jlpcc.jar -i my_script.lpc
Enterint interactive mode. Type '!exit' to leave.
LPC> int i;
LPC> i = 7*4;
```

In this example the script `my_script.lpc` will be loaded and executed. Then you will enter the interactive mode where you can interact with the loaded code.

### Executing Debug Directives
The `jlpcc` tool has a number of built in debug directives. These are inteded to help you debug the state of your code after running it.

The following debug directives are currently supported:

| Directive | Shortcut | What it does |
| --        | --       | --           |
| !print <var> | !p <var> | Prints the current state of a variable in the code. |
| !dump | !d | Writes a full dump of the code object. |
| !code | !c | Outputs the current code executed in the interactive loop. |
| !exit | !e | Exits the interactive mode. |

### !print: Printing Variable Values

  You can print the value of a variable using the following command:

  ```bash
  !print myVariable
  ```

**Example:**
```bash
$ java -jar jlpcc.jar -i hello.lpc
Entering interactive mode. Type '!exit' to leave.
LPC> !print str
LPCVariable{valueExpr='Hello, World!', dType=STRING, mixed=false}
```

This script contains a global string variable `str` with the content `"Hello, World!"`. With `!print str` the variable object is displayed and tells us, that it contains `"Hello, World!"` is a `string` and not a `mixed` variable.

As you can see, these debugger directives expose internal details of the Java objects involved.

### !dump: Dumping Object State

  To see the current state of the object you are interacting with, use:

  ```bash
  !dump
  ```

**Example:**
```bash
$ java -jar jlpcc.jar hello_world.lpc
Entering interactive mode. Type '!exit' to leave.
LPC> !dump
LPCObject{functions={}, globalVariables={str=LPCVariable{valueExpr='Hello, World!', dType=STRING, mixed=false}}}
LPC> 
```

The script `hello_world.lpc` does nothing but define a string `s` with the content `"Hello, World!"`. With `!dump` the whole object state of the script is exposed, listing all global functions and variables registered.

### !code: Viewing Current Code

This directive is useful if you want to get an overview on what is currently happening in the code, especially in longer sessions. 

To print the currently interpreted code, type:

  ```bash
  !code
  ```

**Example:**
```bash
$java -jar jlpcc.jar -i hello.lpc
Entering interactive mode. Type '!exit' to leave.
LPC> !code
0001: string str = "Hello, World!";
LPC> str += " It is full of stars!\n";
LPC> !code
0001: string str = "Hello, World!";
0002: void __interactive_dbg_1728298504468() {
0003: 
0004: str += " It is full of stars!\n";
0005: }
LPC> !p str
```

As you can see the `!code` directive always prints the current stat of the code loaded in interactive mode. The first time we call it, it shows the initial loaded code from `hello.lpc`:

```c
0001: string str = "Hello, World!";
```

After entering additional code and executing `!code` again it shows the injected function that was implicitly created:

```c
0001: string str = "Hello, World!";
0002: void __interactive_dbg_1728298504468() {
0003: 
0004: str += " It is full of stars!\n";
0005: }
```


### !exit: Exiting Interactive Mode

To exit interactive mode, simply type `!exit`. This will return you to the command line.

## Error Handling

If you encounter any errors while running the tool, they will be displayed in the console. Common errors include:

- **File Not Found:** Ensure that the specified LPC file exists in the directory.
- **Syntax Errors:** Check your LPC code for syntax mistakes, as the interpreter will report these during execution.

