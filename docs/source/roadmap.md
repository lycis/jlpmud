# Roadmap
The project is currently purely experimental and will not reach `1.0.0` before a real LPMud can be hosted with it. The current plan is:

Here’s the updated table with the lists consolidated into single cells for each version:

| Version  | Goal                               | Target Features                                                                                                                 | Status |
|----------|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|--|
| **0.0.1** | **Executing LPC Code**              | - Support running basic LPC code with a minimal driver from the `jlpcc` command line utility.<br> - Cover all [basic LPC datatypes](/lpc/001-datatypes.md).<br> - Run flow control within LPC (`for`, `if-else`, `while`, `?-operator`).<br> - Be able to call functions within the same LPC code/object.<br> - Minimal object lifecycle (`load-clone-execute-destruct`).<br> - Calling functions between objects (`->`/`call_other`).<br> - Minimal driver interface to handle basic output (`write` efun) and object registers (to enable the project lifecycle). | ▶️ In Progress |
| **0.0.2** | **Minimal Driver**                  | - Provide a very basic driver that allows connection and interaction with a `R O O T` object.<br> - Have a simple driver interface that runs from the command line.<br> - Basic logging to `STDOUT`.<br> - Handle plain telnet connections (raw text only, no `telnegs`).<br> - Load and route inputs to a master (`R O O T`) object. | ⏸️  Pending |
| **0.0.3** | **efun support**                    | - Support core `efun`s of the LPMud driver.<br> - Provide all common `efun`s to the driver. | 💭 Concept

This format keeps all lists contained in one cell per version while maintaining readability.
